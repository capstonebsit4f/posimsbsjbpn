-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2021 at 01:41 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventorydb`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `fill_calendar` (`start_date` DATE, `end_date` DATE)  BEGIN
  DECLARE crt_date DATE;
  SET crt_date=start_date;
  WHILE crt_date < end_date DO
    INSERT INTO calendar VALUES(crt_date);
    SET crt_date = ADDDATE(crt_date, INTERVAL 1 DAY);
  END WHILE;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `AccountID` int(11) NOT NULL,
  `AccountUsername` varchar(255) NOT NULL,
  `AccountPassword` varchar(255) NOT NULL,
  `AccountFirstName` varchar(255) NOT NULL,
  `AccountLastName` varchar(255) NOT NULL,
  `AccountAddress` varchar(255) NOT NULL,
  `AccountAge` int(50) NOT NULL,
  `AccountType` varchar(255) NOT NULL,
  `AccountStatus` varchar(255) NOT NULL,
  `AccountSex` varchar(255) NOT NULL,
  `AccountContactNo.` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`AccountID`, `AccountUsername`, `AccountPassword`, `AccountFirstName`, `AccountLastName`, `AccountAddress`, `AccountAge`, `AccountType`, `AccountStatus`, `AccountSex`, `AccountContactNo.`) VALUES
(0, '', '', '', '', '', 0, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `account_table`
--

CREATE TABLE `account_table` (
  `AccountID` int(11) NOT NULL,
  `AccountUsername` varchar(50) NOT NULL,
  `AccountPassword` varchar(50) NOT NULL,
  `AccountFirstname` varchar(50) NOT NULL,
  `AccountLastname` varchar(50) NOT NULL,
  `AccountAddress` varchar(100) NOT NULL,
  `AccountAge` int(10) NOT NULL,
  `AccountSex` varchar(20) NOT NULL,
  `AccountContactNo` int(50) NOT NULL,
  `AccountType` varchar(20) NOT NULL,
  `AccountStatus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account_table`
--

INSERT INTO `account_table` (`AccountID`, `AccountUsername`, `AccountPassword`, `AccountFirstname`, `AccountLastname`, `AccountAddress`, `AccountAge`, `AccountSex`, `AccountContactNo`, `AccountType`, `AccountStatus`) VALUES
(1, 'annew28', '25f9e794323b453885f5181f1b624d0b', 'Wenna Roseee', 'BEEEEEE', 'MyPublicADDRESSSSSS', 20, 'Female', 909090988, 'AdminLevel', 'Available'),
(2, 'jaskjas@jdkjsd.com', '25f9e794323b453885f5181f1b624d0b', 'Wenna', 'Caimol', 'jdfuebdjk', 20, 'Female', 2147483647, 'AdminLevel', 'Available'),
(3, 'WEEE', '25f9e794323b453885f5181f1b624d0b', 'Wenna Rose', 'Caimol', 'My Address', 22, 'Male', 2147483647, 'UserLevel', 'Available'),
(4, 'ellaaa', '25f9e794323b453885f5181f1b624d0b', 'wew', 'BEEEEEE', 'jdbfdsj', 20, 'Female', 90909099, 'UserLevel', 'Available');

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE `calendar` (
  `datefield` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `calendar`
--

INSERT INTO `calendar` (`datefield`) VALUES
('2021-01-01'),
('2021-01-02'),
('2021-01-03'),
('2021-01-04'),
('2021-01-05'),
('2021-01-06'),
('2021-01-07'),
('2021-01-08'),
('2021-01-09'),
('2021-01-10'),
('2021-01-11'),
('2021-01-12'),
('2021-01-13'),
('2021-01-14'),
('2021-01-15'),
('2021-01-16'),
('2021-01-17'),
('2021-01-18'),
('2021-01-19'),
('2021-01-20'),
('2021-01-21'),
('2021-01-22'),
('2021-01-23'),
('2021-01-24'),
('2021-01-25'),
('2021-01-26'),
('2021-01-27'),
('2021-01-28'),
('2021-01-29'),
('2021-01-30'),
('2021-01-31'),
('2021-02-01'),
('2021-02-02'),
('2021-02-03'),
('2021-02-04'),
('2021-02-05'),
('2021-02-06'),
('2021-02-07'),
('2021-02-08'),
('2021-02-09'),
('2021-02-10'),
('2021-02-11'),
('2021-02-12'),
('2021-02-13'),
('2021-02-14'),
('2021-02-15'),
('2021-02-16'),
('2021-02-17'),
('2021-02-18'),
('2021-02-19'),
('2021-02-20'),
('2021-02-21'),
('2021-02-22'),
('2021-02-23'),
('2021-02-24'),
('2021-02-25'),
('2021-02-26'),
('2021-02-27'),
('2021-02-28'),
('2021-03-01'),
('2021-03-02'),
('2021-03-03'),
('2021-03-04'),
('2021-03-05'),
('2021-03-06'),
('2021-03-07'),
('2021-03-08'),
('2021-03-09'),
('2021-03-10'),
('2021-03-11'),
('2021-03-12'),
('2021-03-13'),
('2021-03-14'),
('2021-03-15'),
('2021-03-16'),
('2021-03-17'),
('2021-03-18'),
('2021-03-19'),
('2021-03-20'),
('2021-03-21'),
('2021-03-22'),
('2021-03-23'),
('2021-03-24'),
('2021-03-25'),
('2021-03-26'),
('2021-03-27'),
('2021-03-28'),
('2021-03-29'),
('2021-03-30'),
('2021-03-31'),
('2021-04-01'),
('2021-04-02'),
('2021-04-03'),
('2021-04-04'),
('2021-04-05'),
('2021-04-06'),
('2021-04-07'),
('2021-04-08'),
('2021-04-09'),
('2021-04-10'),
('2021-04-11'),
('2021-04-12'),
('2021-04-13'),
('2021-04-14'),
('2021-04-15'),
('2021-04-16'),
('2021-04-17'),
('2021-04-18'),
('2021-04-19'),
('2021-04-20'),
('2021-04-21'),
('2021-04-22'),
('2021-04-23'),
('2021-04-24'),
('2021-04-25'),
('2021-04-26'),
('2021-04-27'),
('2021-04-28'),
('2021-04-29'),
('2021-04-30'),
('2021-05-01'),
('2021-05-02'),
('2021-05-03'),
('2021-05-04'),
('2021-05-05'),
('2021-05-06'),
('2021-05-07'),
('2021-05-08'),
('2021-05-09'),
('2021-05-10'),
('2021-05-11'),
('2021-05-12'),
('2021-05-13'),
('2021-05-14'),
('2021-05-15'),
('2021-05-16'),
('2021-05-17'),
('2021-05-18'),
('2021-05-19'),
('2021-05-20'),
('2021-05-21'),
('2021-05-22'),
('2021-05-23'),
('2021-05-24'),
('2021-05-25'),
('2021-05-26'),
('2021-05-27'),
('2021-05-28'),
('2021-05-29'),
('2021-05-30'),
('2021-05-31'),
('2021-06-01'),
('2021-06-02'),
('2021-06-03'),
('2021-06-04'),
('2021-06-05'),
('2021-06-06'),
('2021-06-07'),
('2021-06-08'),
('2021-06-09'),
('2021-06-10'),
('2021-06-11'),
('2021-06-12'),
('2021-06-13'),
('2021-06-14'),
('2021-06-15'),
('2021-06-16'),
('2021-06-17'),
('2021-06-18'),
('2021-06-19'),
('2021-06-20'),
('2021-06-21'),
('2021-06-22'),
('2021-06-23'),
('2021-06-24'),
('2021-06-25'),
('2021-06-26'),
('2021-06-27'),
('2021-06-28'),
('2021-06-29'),
('2021-06-30'),
('2021-07-01'),
('2021-07-02'),
('2021-07-03'),
('2021-07-04'),
('2021-07-05'),
('2021-07-06'),
('2021-07-07'),
('2021-07-08'),
('2021-07-09'),
('2021-07-10'),
('2021-07-11'),
('2021-07-12'),
('2021-07-13'),
('2021-07-14'),
('2021-07-15'),
('2021-07-16'),
('2021-07-17'),
('2021-07-18'),
('2021-07-19'),
('2021-07-20'),
('2021-07-21'),
('2021-07-22'),
('2021-07-23'),
('2021-07-24'),
('2021-07-25'),
('2021-07-26'),
('2021-07-27'),
('2021-07-28'),
('2021-07-29'),
('2021-07-30'),
('2021-07-31'),
('2021-08-01'),
('2021-08-02'),
('2021-08-03'),
('2021-08-04'),
('2021-08-05'),
('2021-08-06'),
('2021-08-07'),
('2021-08-08'),
('2021-08-09'),
('2021-08-10'),
('2021-08-11'),
('2021-08-12'),
('2021-08-13'),
('2021-08-14'),
('2021-08-15'),
('2021-08-16'),
('2021-08-17'),
('2021-08-18'),
('2021-08-19'),
('2021-08-20'),
('2021-08-21'),
('2021-08-22'),
('2021-08-23'),
('2021-08-24'),
('2021-08-25'),
('2021-08-26'),
('2021-08-27'),
('2021-08-28'),
('2021-08-29'),
('2021-08-30'),
('2021-08-31'),
('2021-09-01'),
('2021-09-02'),
('2021-09-03'),
('2021-09-04'),
('2021-09-05'),
('2021-09-06'),
('2021-09-07'),
('2021-09-08'),
('2021-09-09'),
('2021-09-10'),
('2021-09-11'),
('2021-09-12'),
('2021-09-13'),
('2021-09-14'),
('2021-09-15'),
('2021-09-16'),
('2021-09-17'),
('2021-09-18'),
('2021-09-19'),
('2021-09-20'),
('2021-09-21'),
('2021-09-22'),
('2021-09-23'),
('2021-09-24'),
('2021-09-25'),
('2021-09-26'),
('2021-09-27'),
('2021-09-28'),
('2021-09-29'),
('2021-09-30'),
('2021-10-01'),
('2021-10-02'),
('2021-10-03'),
('2021-10-04'),
('2021-10-05'),
('2021-10-06'),
('2021-10-07'),
('2021-10-08'),
('2021-10-09'),
('2021-10-10'),
('2021-10-11'),
('2021-10-12'),
('2021-10-13'),
('2021-10-14'),
('2021-10-15'),
('2021-10-16'),
('2021-10-17'),
('2021-10-18'),
('2021-10-19'),
('2021-10-20'),
('2021-10-21'),
('2021-10-22'),
('2021-10-23'),
('2021-10-24'),
('2021-10-25'),
('2021-10-26'),
('2021-10-27'),
('2021-10-28'),
('2021-10-29'),
('2021-10-30'),
('2021-10-31'),
('2021-11-01'),
('2021-11-02'),
('2021-11-03'),
('2021-11-04'),
('2021-11-05'),
('2021-11-06'),
('2021-11-07'),
('2021-11-08'),
('2021-11-09'),
('2021-11-10'),
('2021-11-11'),
('2021-11-12'),
('2021-11-13'),
('2021-11-14'),
('2021-11-15'),
('2021-11-16'),
('2021-11-17'),
('2021-11-18'),
('2021-11-19'),
('2021-11-20'),
('2021-11-21'),
('2021-11-22'),
('2021-11-23'),
('2021-11-24'),
('2021-11-25'),
('2021-11-26'),
('2021-11-27'),
('2021-11-28'),
('2021-11-29'),
('2021-11-30'),
('2021-12-01'),
('2021-12-02'),
('2021-12-03'),
('2021-12-04'),
('2021-12-05'),
('2021-12-06'),
('2021-12-07'),
('2021-12-08'),
('2021-12-09'),
('2021-12-10'),
('2021-12-11'),
('2021-12-12'),
('2021-12-13'),
('2021-12-14'),
('2021-12-15'),
('2021-12-16'),
('2021-12-17'),
('2021-12-18'),
('2021-12-19'),
('2021-12-20'),
('2021-12-21'),
('2021-12-22'),
('2021-12-23'),
('2021-12-24'),
('2021-12-25'),
('2021-12-26'),
('2021-12-27'),
('2021-12-28'),
('2021-12-29'),
('2021-12-30');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `CartID` int(50) NOT NULL,
  `AccountID` int(50) NOT NULL,
  `ProductID` int(50) NOT NULL,
  `qty` int(50) NOT NULL,
  `qtyPrice` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`CartID`, `AccountID`, `ProductID`, `qty`, `qtyPrice`) VALUES
(77, 3, 20100002, 1, 150),
(78, 1, 20100011, 2, 300),
(79, 1, 30300016, 1, 250),
(80, 1, 30300013, 1, 250);

-- --------------------------------------------------------

--
-- Table structure for table `notif`
--

CREATE TABLE `notif` (
  `notifID` int(11) NOT NULL,
  `notifValue` int(100) NOT NULL,
  `notifBool` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notif`
--

INSERT INTO `notif` (`notifID`, `notifValue`, `notifBool`) VALUES
(1, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(255) NOT NULL,
  `ProductDescription` varchar(255) NOT NULL,
  `ProductStatus` varchar(255) NOT NULL,
  `ProductQuantity` int(50) NOT NULL,
  `ProductType` varchar(255) NOT NULL,
  `ProductPrice` varchar(255) NOT NULL,
  `ProductSupplierCost` int(50) NOT NULL,
  `SupplierID` int(50) NOT NULL
 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductID`, `ProductName`, `ProductDescription`, `ProductStatus`, `ProductQuantity`, `ProductType`, `ProductPrice`, `ProductSupplierCost`, `SupplierID`) VALUES
(20100001, 'HBD Foil Balloon GOLD Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 10, 'Foil Balloon', '150.00', 85, 40100001),
(20100002, 'HBD Foil Balloon SILVER Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 52, 'Foil Balloon', '150.00', 0, 40100001),
(20100003, 'HBD Foil Balloon ROSE GOLD Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100004, 'HBD Foil Balloon BLUE Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100005, 'HBD Foil Balloon BLACK Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100006, 'HBD Foil Balloon PINK Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 10, 'Foil Balloon', '150.00', 85, 40100001),
(20100007, 'HBD Foil Balloon PINK Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100008, 'HBD Foil Balloon RED Metalli', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100009, 'HBD Foil Balloon POLKA PINK Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 4, 'Foil Balloon', '150.00', 85, 40100001),
(20100010, 'HBD Foil Balloon POLKA BLUE Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100011, 'HBD Foil Balloon RED & BLACK Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100012, 'HBD Foil Balloon MULTICOLOR Metallic', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 10, 'Foil Balloon', '150.00', 85, 40100001),
(20100013, 'HBD Foil Balloon PINK Pastel', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100014, 'HBD Foil Balloon BLUE Pastel', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100015, 'HBD Foil Balloon ASSORTED PURPLE Pastel', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100016, 'HBD Foil Balloon MULTICOLOR Pastel', ' 16 inch Letters HAPPY BIRTHDAY Foil Balloons 13 pcs', 'Good', 5, 'Foil Balloon', '150.00', 85, 40100001),
(20100017, 'HBD Cursive Foil Balloon GOLD', '16 inch Letter Foil Balloon Cursive  2 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100018, 'HBD Cursive Foil Balloon ROSE GOLD', '16 inch Letter Foil Balloon Cursive  2 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100019, 'HBD Cursive Foil Balloon SILVER', '16 inch Letter Foil Balloon Cursive  2 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100020, 'HBD Cursive Foil Balloon RED', '16 inch Letter Foil Balloon Cursive  2 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100021, 'HBD Cursive Foil Balloon PINK', '16 inch Letter Foil Balloon Cursive  2 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100022, 'Happy Anniversary Foil Balloon Set GOLD', '16 inch Letters Foil Balloon 16 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100023, 'Happy Anniversary Foil Balloon Set SILVER', '16 inch Letters Foil Balloon 16 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100024, 'Happy Anniversary Foil Balloon Set ROSE GOLD', '16 inch Letters HAPPY ANNIVERSARY Foil Balloon 16 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100025, 'Happy Christening Foil Balloon Set Printed BLUE', '16 inch Letters HAPPY CHRISTENING Foil Balloon 16 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100026, 'Happy Christening Foil Balloon Set Printed PINK', '16 inch Letters HAPPY CHRISTENING Foil Balloon 16 pcs', 'Good', 5, 'Foil Balloon', '120.00', 100, 40100001),
(20100027, 'CONGRATS Foil Balloon Set GOLD', '16 inch CONGRATS Letters Foil Balloon 16 pcs', 'Good', 5, 'Foil Balloon', '120.00', 90, 40100001),
(20200001, 'Happy Birthday  Mini Plain Black', 'Happy birthday banner with Gold Print and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 5, 'Banner', '80.00', 25, 40100001),
(20200002, 'Happy Birthday  Mini Plain White', 'Happy birthday banner with Gold Print and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 9, 'Banner', '60.00', 25, 40100001),
(20200003, 'Happy Birthday  Mini Plain  Blue', 'Happy birthday banner with Gold Print and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '60.00', 25, 40100001),
(20200004, 'Happy Birthday  Mini Plain  Red', 'Happy birthday banner with Gold Print and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '60.00', 25, 40100001),
(20200005, 'Happy Birthday  Mini Plain  Violet', 'Happy birthday banner with Gold Print and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '60.00', 25, 40100001),
(20200006, 'Happy Birthday  Mini Plain  Pink', 'Happy birthday banner with Gold Print and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '60.00', 25, 40100001),
(20200007, 'Happy Birthday  Mini Plain  Mint Green', 'Happy birthday banner with Gold Print and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '60.00', 25, 40100001),
(20200008, 'Happy Birthday  Mini Plain  Light Green', 'Happy birthday banner with Gold Print and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '60.00', 25, 40100001),
(20200009, 'Happy Birthday  Mini Glittered  Gold', 'Happy birthday banner with Silver Font and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '60.00', 35, 40100001),
(20200010, 'Happy Birthday  Mini Glittered  Silver', 'Happy birthday banner with Gold Font and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '70.00', 35, 40100001),
(20200011, 'Happy Birthday  Mini Glittered  Black', 'Happy birthday banner with Gold Font and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '70.00', 35, 40100001),
(20200012, 'Happy Birthday  Mini Glittered  Blue', 'Happy birthday banner with Gold Font and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '70.00', 35, 40100001),
(20200013, 'Happy Birthday  Mini Glittered  Rose Gold', 'Happy birthday banner with Gold Font and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '70.00', 35, 40100001),
(20200014, 'Happy Birthday  Mini Glittered  Pink', 'Happy birthday banner with Gold Font and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '70.00', 35, 40100001),
(20200015, 'Happy Birthday  Mini Glittered  Fuchsia Pink', 'Happy birthday banner with Gold Font and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '70.00', 35, 40100001),
(20200016, 'Happy Birthday Polka dots Black', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200017, 'Happy Birthday Polka dots Yellow', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200018, 'Happy Birthday Polka dots Light Blue', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200019, 'Happy Birthday Polka dots Grey', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200020, 'Happy Birthday Polka dots Red', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200021, 'Happy Birthday Polka dots Pink', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200022, 'Happy Birthday Polka dots Orange', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200023, 'Happy Birthday Polka dots Green', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200024, 'Happy Birthday Polka dots Violet', 'Happy birthday banner with polka dot desing and satin ribbon / 13 pcs letters 4.5x6.5 inches', 'Good', 10, 'Banner', '50.00', 20, 40100001),
(20200025, 'Happy Birthday Glittered Banner Black and Gold', 'Happy birthday Big banner with dotted glittered / 13 pcs letters', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200026, 'Happy Birthday Glittered Banner Black and Blue', 'Happy birthday Big banner with dotted glittered / 13 pcs letters ', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200027, 'Happy Birthday Glittered Banner Black and Pink', 'Happy birthday Big banner with dotted glittered / 13 pcs letters ', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200028, 'Happy Birthday Glittered Banner Pink and White', 'Happy birthday Big banner with dotted glittered / 13 pcs letters ', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200029, 'Happy Birthday Glittered Banner Stripes White', 'Happy birthday Big banner with stripes glittered / 13 pcs letters ', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200030, 'Happy Birthday Glittered Banner Stripes Pink', 'Happy birthday Big banner with dotted glittered / 13 pcs letters', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200031, 'Happy Birthday Glittered Banner Polka Pink', 'Happy birthday Big banner with dotted glittered / 13 pcs letters', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200032, 'Happy Birthday  Glittered Gold Banner with Gradient Font', 'Happy birthday Big banner with dotted glittered / 13 pcs letters ', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200033, 'Happy Birthday  Glittered Silver Banner with Gradient Font', 'Happy birthday Big banner with dotted glittered / 13 pcs letters ', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200034, 'Happy Birthday  Glittered Blue Banner with Gradient Font', 'Happy birthday Big banner with dotted glittered / 13 pcs letters ', 'Good', 10, 'Banner', '100.00', 50, 40100001),
(20200035, 'Happy Birthday Banner Pastel', 'Happy birthday Pastel Color Letters banner / 13 pcs 13x18.5cm', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200036, 'Happy Birthday  Banner MOANA', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200037, 'Happy Birthday Banner MINNIE MOUSE', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200038, 'Happy Birthday Banner SOFIA THE FIRST', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200039, 'Happy Birthday Banner DISNEY PRINCESS', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200040, 'Happy Birthday Banner LOL SURPRISE', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200041, 'Happy Birthday Banner HELLO KITTY', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200042, 'Happy Birthday Banner BARBIE', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200043, 'Happy Birthday Banner PEPPA PIG', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200044, 'Happy Birthday Banner UNICORN', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200045, 'Happy Birthday Banner MERMAID', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200046, 'Happy Birthday Banner BASKETBALL', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200047, 'Happy Birthday Banner COCOMELON', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200048, 'Happy Birthday Banner BOSS BABY', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200049, 'Happy Birthday Banner SPIDERMAN', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20200050, 'Happy Birthday Banner AMONG US', 'Happy birthday theme banner / 13 pcs cut out shapes / Multicolor', 'Good', 10, 'Banner', '80.00', 35, 40100001),
(20300001, 'MOANA 5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300002, 'LADY M.  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300003, 'MINNIE MOUSE  5in1 Foil Balloon Set', 'Set of Character shape (1), Round (2) and Heart (2) Foil Mylar Balloons', 'Good', 25, 'Foil Balloon', '120.00', 75, 40100001),
(20300004, 'SOFIA THE FIRST  5in1 Foil Balloon Set', 'Set of Character shape (1), Round (2) and Heart (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300005, 'LOL SURPRISE  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300006, 'HELLO KITTY  5in1 Foil Balloon Set', 'Set of Character shape (1), Round (2) and Heart (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300007, 'BARBIE  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300008, 'PEPPA PIG  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300009, 'UNICORN  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Heart (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300010, 'MERMAID  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Heart (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300011, 'FROZEN  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Heart (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300012, 'FLAMINGO  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Heart (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300013, 'MICKEY MOUSE  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300014, 'DINOSAUR  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300015, 'BASKETBALL  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300016, 'PAW PATROL  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20300017, 'SPONGE BOB  5in1 Foil Balloon Set', ' Set of Character shape (1), Round (2) and Star (2) Foil Mylar Balloons', 'Good', 5, 'Foil Balloon', '120.00', 75, 40100001),
(20400001, 'MOANA Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 9, 'Flag', '40.00', 20, 40100001),
(20400002, 'MINNIE MOUSE Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400003, 'SOFIA THE FIRST Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400004, 'DISNEY PRINCESS Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400005, 'LOL SURPRISE Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400006, 'HELLO KITTY Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400007, 'BARBIE Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400008, 'PEPPA PIG Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400009, 'UNICORN Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400010, 'FROZEN Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400011, 'FLAMINGO Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400012, 'LITTLE PONY Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400013, 'MICKEY MOUSE Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400014, 'SOCCER Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400015, 'COCOMELON Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400016, 'PAW PATROL Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400017, 'SAFARI/JUNGLE Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400018, 'SPONGE BOB Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400019, 'BOSS BABY Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400020, 'BATMAN Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400021, 'JUSTICE LEAGUE Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400022, 'AVENGERS Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400023, 'SPIDERMAN Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400024, 'CARS Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400025, 'POLKA DOTS BLUE Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400026, 'POLKA DOTS PINK Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20400027, 'POLKA DOTS RED Party Flags', ' 1 pack/10 paper triangular flags 3M', 'Good', 10, 'Flag', '40.00', 20, 40100001),
(20500001, 'MOANA Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500002, 'MINNIE MOUSE Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500003, 'SOFIA THE FIRST Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500004, 'DISNEY PRINCESS Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500005, 'LOL SURPRISE Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500006, 'HELLO KITTY Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500007, 'PEPPA PIG Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500008, 'UNICORN Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500009, 'FROZEN Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500010, 'LITTLE PONY Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500011, 'MICKEY MOUSE Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500012, 'COCOMELON Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500013, 'PAW PATROL Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500014, 'SAFARI/JUNGLE Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500015, 'BOSS BABY Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500016, 'BATMAN Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500017, 'SUPERMAN Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500018, 'AVENGERS Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500019, 'MINIONS Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500020, 'SPIDERMAN Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500021, 'PJ MASK Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500022, 'CARS Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500023, 'POLKA DOTS RED Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500024, 'POLKA DOTS PINK Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20500025, 'POLKA DOTS BLUE Party Hat', '1 pack/10 pcs Birthday hat character printed / freesize', 'Good', 10, 'Bag', '35.00', 18, 40100001),
(20700001, 'MOANA Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700002, 'MINNIE MOUSE Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700003, 'SOFIA THE FIRST Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700004, 'DISNEY PRINCESS Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700005, 'LOL SURPRISE Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700006, 'HELLO KITTY Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700007, 'BARBIE Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700008, 'PEPPA PIG Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700009, 'UNICORN Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700010, 'FROZEN Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700011, 'FLAMINGO Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700012, 'LITTLE PONY Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700013, 'MICKEY MOUSE Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700014, 'SOCCER Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700015, 'COCOMELON Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700016, 'PAW PATROL Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700017, 'SAFARI/JUNGLE Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700018, 'SPONGE BOB Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700019, 'BOSS BABY Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700020, 'BATMAN Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700021, 'JUSTICE LEAGUE Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700022, 'AVENGERS Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700023, 'SPIDERMAN Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700024, 'POLKA DOTS BLUE Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700025, 'POLKA DOTS PINK Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700026, 'POLKA DOTS RED Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 8, 'Bag', '25.00', 15, 40100001),
(20700027, 'POLKA DOTS GREEN Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700028, 'POLKA DOTS YELLOW Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20700029, 'POLKA DOTS PURPLE Loot Bag', '1 pack/10 pcs plastic loot bags 17x25cm', 'Good', 10, 'Bag', '25.00', 15, 40100001),
(20800001, 'MOANA Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 24, 'Stand', '120.00', 60, 40100001),
(20800002, 'MINNIE MOUSE Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800003, 'SOFIA THE FIRST Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800004, 'DISNEY PRINCESS Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 332, 'Stand', '120.00', 60, 40100001),
(20800005, 'LOL SURPRISE Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800006, 'HELLO KITTY Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800007, 'BARBIE Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800008, 'PEPPA PIG Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800009, 'UNICORN Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800010, 'FROZEN Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800011, 'LITTLE PONY Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800012, 'MICKEY MOUSE Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800013, 'PAW PATROL Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800014, 'SAFARI/JUNGLE Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800015, 'BOSS BABY Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 7, 'Stand', '120.00', 60, 40100001),
(20800016, 'AVENGERS Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800017, 'SPIDERMAN Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800018, 'CARS Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800019, 'POLKA DOTS BLUE Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800020, 'POLKA DOTS L.BLUE Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800021, 'POLKA DOTS Yellow Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800022, 'POLKA DOTS PINK Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800023, 'PLAIN GOLD Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800024, 'PLAIN SILVER Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(20800025, 'PLAIN PINK Cupcake Stand', 'Character Printed 3 Cupcake Stand 3 Tier Board', 'Good', 10, 'Stand', '120.00', 60, 40100001),
(30100001, '5\" Ordinary Balloon RED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100002, '5\" Ordinary Balloon L-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100003, '5\" Ordinary Balloon D-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100004, '5\" Ordinary Balloon WHITE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100005, '5\" Ordinary Balloon D-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100006, '5\" Ordinary Balloon L-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100007, '5\" Ordinary Balloon TEAL', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100008, '5\" Ordinary Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100009, '5\" Ordinary Balloon D-GRREN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100010, '5\" Ordinary Balloon L-GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100011, '5\" Ordinary Balloon ORANGE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100012, '5\" Ordinary Balloon GOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100013, '5\" Ordinary Balloon ROSEGOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100014, '5\" Ordinary Balloon SILVER', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100015, '5\" Ordinary Balloon BRONZE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100016, '5\" Ordinary Balloon BROWN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100017, '5\" Ordinary Balloon VIOLET', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100018, '5\" Ordinary Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100019, '5\" Ordinary Balloon BLACK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100020, '5\" Pastel Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100021, '5\" Pastel Balloon PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100022, '5\" Pastel Balloon RED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100023, '5\" Pastel Balloon GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100024, '5\" Pastel Balloon PURPLE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100025, '5\" Pastel Balloon BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100026, '5\" Pastel Balloon PEACH', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100027, '5\" Pastel Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100028, '5\" Metallic Balloon RED ', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100029, '5\" Metallic Balloon L-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100030, '5\" Metallic Balloon D-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100031, '5\" Metallic Balloon WHITE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100032, '5\" Metallic Balloon D-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100033, '5\" Metallic Balloon L-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100034, '5\" Metallic Balloon TEAL', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100035, '5\" Metallic Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100036, '5\" Metallic Balloon D-GRREN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100037, '5\" Metallic Balloon L-GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100038, '5\" Metallic Balloon ORANGE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100039, '5\" Metallic Balloon GOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100040, '5\" Metallic Balloon ROSEGOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100041, '5\" Metallic Balloon SILVER', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100042, '5\" Metallic Balloon BRONZE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100043, '5\" Metallic Balloon BROWN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100044, '5\" Metallic Balloon VIOLET', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100045, '5\" Metallic Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30100046, '5\" Metallic BalloonBLACK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '150.00', 80, 40100001),
(30200001, '10\" Ordinary Balloon RED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200002, '10\" Ordinary Balloon L-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200003, '10\" Ordinary Balloon D-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200004, '10\" Ordinary Balloon WHITE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200005, '10\" Ordinary Balloon D-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200006, '10\" Ordinary Balloon L-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200007, '10\" Ordinary Balloon TEAL', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200008, '10\" Ordinary Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200009, '10\" Ordinary Balloon D-GRREN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200010, '10\" Ordinary Balloon L-GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200011, '10\" Ordinary Balloon ORANGE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200012, '10\" Ordinary Balloon GOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200013, '10\" Ordinary Balloon ROSEGOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200014, '10\" Ordinary Balloon SILVER', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200015, '10\" Ordinary Balloon BRONZE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200016, '10\" Ordinary Balloon BROWN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200017, '10\" Ordinary Balloon VIOLET', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200018, '10\" Ordinary Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200019, '10\" Ordinary Balloon BLACK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200020, '10\" Pastel Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200021, '10\" Pastel Balloon PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200022, '10\" Pastel Balloon RED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200023, '10\" Pastel Balloon GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200024, '10\" Pastel Balloon PURPLE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200025, '10\" Pastel Balloon BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200026, '10\" Pastel Balloon PEACH', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200027, '10\" Pastel Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200028, '10\" Metallic Balloon RED ', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200029, '10\" Metallic Balloon L-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200030, '10\" Metallic Balloon D-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200031, '10\" Metallic Balloon WHITE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200032, '10\" Metallic Balloon D-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200033, '10\" Metallic Balloon L-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200034, '10\" Metallic Balloon TEAL', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200035, '10\" Metallic Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200036, '10\" Metallic Balloon D-GRREN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200037, '10\" Metallic Balloon L-GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200038, '10\" Metallic Balloon ORANGE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200039, '10\" Metallic Balloon GOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200040, '10\" Metallic Balloon ROSEGOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200041, '10\" Metallic Balloon SILVER', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200042, '10\" Metallic Balloon BRONZE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200043, '10\" Metallic Balloon BROWN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200044, '10\" Metallic Balloon VIOLET', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200045, '10\" Metallic Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30200046, '10\" Metallic BalloonBLACK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '200.00', 120, 40100001),
(30300001, '12\" Ordinary Balloon RED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300002, '12\" Ordinary Balloon L-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300003, '12\" Ordinary Balloon D-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300004, '12\" Ordinary Balloon WHITE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300005, '12\" Ordinary Balloon D-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300006, '12\" Ordinary Balloon L-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300007, '12\" Ordinary Balloon TEAL', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300008, '12\" Ordinary Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300009, '12\" Ordinary Balloon D-GRREN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300010, '12\" Ordinary Balloon L-GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300011, '12\" Ordinary Balloon ORANGE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300012, '12\" Ordinary Balloon GOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300013, '12\" Ordinary Balloon ROSEGOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300014, '12\" Ordinary Balloon SILVER', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300015, '12\" Ordinary Balloon BRONZE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300016, '12\" Ordinary Balloon BROWN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300017, '12\" Ordinary Balloon VIOLET', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300018, '12\" Ordinary Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300019, '12\" Ordinary Balloon BLACK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300020, '12\" Pastel Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300021, '12\" Pastel Balloon PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300022, '12\" Pastel Balloon RED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300023, '12\" Pastel Balloon GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300024, '12\" Pastel Balloon PURPLE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300025, '12\" Pastel Balloon BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300026, '12\" Pastel Balloon PEACH', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300027, '12\" Pastel Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300028, '12\" Metallic Balloon RED ', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300029, '12\" Metallic Balloon L-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300030, '12\" Metallic Balloon D-BLUE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300031, '12\" Metallic Balloon WHITE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300032, '12\" Metallic Balloon D-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300033, '12\" Metallic Balloon L-PINK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300034, '12\" Metallic Balloon TEAL', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300035, '12\" Metallic Balloon YELLOW', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300036, '12\" Metallic Balloon D-GRREN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300037, '12\" Metallic Balloon L-GREEN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300038, '12\" Metallic Balloon ORANGE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300039, '12\" Metallic Balloon GOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300040, '12\" Metallic Balloon ROSEGOLD', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300041, '12\" Metallic Balloon SILVER', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300042, '12\" Metallic Balloon BRONZE', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300043, '12\" Metallic Balloon BROWN', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300044, '12\" Metallic Balloon VIOLET', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300045, '12\" Metallic Balloon ASSORTED', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001),
(30300046, '12\" Metallic BalloonBLACK', '1 pack/100 pcs standard balloon', 'Good', 10, 'Balloon', '250.00', 150, 40100001);

-- --------------------------------------------------------

--
-- Table structure for table `product_temp`
--

CREATE TABLE `product_temp` (
  `Product_tempID` int(11) NOT NULL,
  `ProductName` varchar(255) NOT NULL,
  `ProductDescription` varchar(255) NOT NULL,
  `ProductStatus` varchar(255) NOT NULL,
  `ProductQuantity` int(50) NOT NULL,
  `ProductType` varchar(255) NOT NULL,
  `ProductPrice` varchar(255) NOT NULL,
  `ProductSupplierCost` int(50) NOT NULL,
  `SupplierID` int(50) NOT NULL,
  `AccountID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `restock`
--

CREATE TABLE `restock` (
  `RestockID` int(11) NOT NULL,
  `RestockTotalQuantity` int(50) NOT NULL,
  `RestockDate` datetime NOT NULL,
  `RestockTotalCost` int(50) NOT NULL,
  `RestockTotalDMGDProducts` int(50) NOT NULL,
  `TotalunDMGDQty` int(20) NOT NULL,
  `CountProduct` int(20) NOT NULL,
  `AccountID` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restock`
--

INSERT INTO `restock` (`RestockID`, `RestockTotalQuantity`, `RestockDate`, `RestockTotalCost`, `RestockTotalDMGDProducts`, `TotalunDMGDQty`, `CountProduct`, `AccountID`) VALUES
(2, 256, '2021-05-28 00:35:14', 4741, 41, 215, 14, 1),
(3, 65, '2021-06-02 20:18:48', 9194, 20, 45, 3, 1),
(4, 334, '2021-06-04 17:41:14', 26720, 2, 332, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `restockdetail`
--

CREATE TABLE `restockdetail` (
  `restockDetailID` int(11) NOT NULL,
  `RestockID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Added_qty` int(11) NOT NULL,
  `UnDMGD_qty` int(11) NOT NULL,
  `DMGD_qty` int(11) NOT NULL,
  `UnitCost` int(11) NOT NULL,
  `SupplierID` int(11) NOT NULL,
  `subTotalCost` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restockdetail`
--

INSERT INTO `restockdetail` (`restockDetailID`, `RestockID`, `ProductID`, `Added_qty`, `UnDMGD_qty`, `DMGD_qty`, `UnitCost`, `SupplierID`, `subTotalCost`) VALUES
(1, 2, 20100001, 5, 3, 2, 22, 0, 110),
(2, 2, 20100008, 34, 22, 12, 33, 0, 1122),
(3, 2, 20100009, 13, 13, 0, 3, 0, 39),
(4, 2, 20100002, 23, 22, 1, 23, 0, 529),
(5, 2, 20100007, 12, 0, 12, 12, 0, 144),
(6, 2, 20100010, 22, 20, 2, 23, 0, 506),
(7, 2, 20100003, 11, 1, 10, 12, 12, 132),
(8, 2, 20100004, 3, 2, 1, 23, 23, 69),
(9, 2, 20500014, 11, 10, 1, 11, 12, 121),
(10, 2, 20800025, 22, 22, 0, 2, 0, 44),
(11, 2, 20700018, 21, 21, 0, 32, 0, 672),
(12, 2, 20100005, 3, 3, 0, 99, 0, 297),
(13, 2, 30200020, 32, 32, 0, 23, 0, 736),
(14, 2, 20200050, 44, 44, 0, 5, 4, 220),
(15, 3, 20800001, 39, 21, 18, 200, 4010000, 7800),
(16, 3, 20300003, 22, 20, 2, 23, 40100022, 506),
(17, 3, 20800015, 4, 4, 0, 222, 2222, 888),
(18, 4, 20800004, 334, 332, 2, 80, 400001, 26720);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `SalesID` int(11) NOT NULL,
  `SalesDate` datetime NOT NULL,
  `SalesTotal` int(50) NOT NULL,
  `SalesTotalQty` int(11) NOT NULL,
  `SalesCountProduct` int(20) NOT NULL,
  `AccountID` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`SalesID`, `SalesDate`, `SalesTotal`, `SalesTotalQty`, `SalesCountProduct`, `AccountID`) VALUES
(2, '2021-05-28 01:25:05', 1350, 9, 2, 1),
(3, '2021-05-28 22:09:02', 150, 1, 1, 1),
(4, '2021-05-28 22:09:24', 450, 3, 1, 1),
(5, '2021-05-28 22:09:43', 150, 1, 1, 1),
(6, '2021-05-28 22:10:04', 150, 1, 1, 1),
(7, '2021-05-28 22:10:33', 75, 3, 1, 1),
(8, '2021-05-28 22:11:04', 900, 6, 1, 1),
(9, '2021-05-28 22:11:40', 300, 2, 1, 1),
(10, '2021-05-28 22:12:14', 75, 3, 1, 1),
(11, '2021-06-01 20:32:34', 3300, 22, 5, 3),
(12, '2021-06-01 21:38:35', 40, 1, 1, 3),
(13, '2021-06-04 17:39:36', 325, 6, 3, 1),
(14, '2021-06-15 01:04:38', 750, 5, 1, 1),
(15, '2021-06-15 18:51:41', 750, 5, 2, 1),
(16, '2021-06-15 19:45:02', 1050, 7, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `salesdetail`
--

CREATE TABLE `salesdetail` (
  `SalesDetailID` int(11) NOT NULL,
  `SalesID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Sales_qty` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesdetail`
--

INSERT INTO `salesdetail` (`SalesDetailID`, `SalesID`, `ProductID`, `Sales_qty`) VALUES
(1, 1, 20100002, 1),
(2, 1, 20700006, 2),
(12, 2, 20100010, 5),
(13, 2, 20100008, 4),
(14, 3, 20100002, 1),
(15, 4, 20100008, 3),
(16, 5, 20100008, 1),
(17, 6, 20100012, 1),
(18, 7, 20700026, 3),
(19, 8, 20100008, 6),
(20, 9, 20100012, 2),
(21, 10, 20700025, 3),
(22, 11, 20100002, 15),
(23, 11, 20100005, 1),
(24, 11, 20100010, 4),
(25, 11, 20100016, 1),
(26, 11, 30100034, 1),
(27, 12, 20400001, 1),
(28, 13, 20700026, 2),
(29, 13, 20800004, 3),
(30, 13, 30200038, 1),
(31, 14, 20100001, 5),
(32, 15, 20100002, 3),
(33, 15, 20100006, 2),
(34, 16, 20100012, 5),
(35, 16, 20200002, 1),
(36, 16, 20100009, 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `SupplierID` int(11) NOT NULL,
  `SupplierName` varchar(150) NOT NULL,
  `SupplierAddress` varchar(255) NOT NULL,
  `SupplierContactNo.` int(255) NOT NULL,
  `AccountID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`SupplierID`, `SupplierName`, `SupplierAddress`, `SupplierContactNo.`, `AccountID`) VALUES
(40100016, 'We', 'Blk Update Lot 00 True Boolean', 2147483647, 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_temp`
--

CREATE TABLE `supplier_temp` (
  `SupplierID` int(11) NOT NULL,
  `SupplierName` varchar(100) NOT NULL,
  `SupplierAddress` varchar(150) NOT NULL,
  `SupplierContactNo` varchar(20) NOT NULL,
  `AccountID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `temprestock`
--

CREATE TABLE `temprestock` (
  `tempRestockID` int(50) NOT NULL,
  `AccountID` int(50) NOT NULL,
  `ProductID` int(50) NOT NULL,
  `SupplierID` int(50) NOT NULL,
  `UnitCost` int(50) NOT NULL,
  `ProductState` varchar(50) NOT NULL,
  `UndamagedQty` int(50) NOT NULL,
  `qty` int(50) NOT NULL,
  `qtyPrice` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `TransactionID` int(11) NOT NULL,
  `TransactionQuantity` int(50) NOT NULL,
  `TransactionTotal` double NOT NULL,
  `TransactionType` varchar(255) NOT NULL,
  `TransactionDate` datetime NOT NULL,
  `ProductID` int(50) NOT NULL,
  `AccountID` int(50) NOT NULL,
  `SalesID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`TransactionID`, `TransactionQuantity`, `TransactionTotal`, `TransactionType`, `TransactionDate`, `ProductID`, `AccountID`, `SalesID`) VALUES
(1, 1, 150, '', '2021-05-26 22:49:19', 20100002, 1, 1),
(2, 2, 50, '', '2021-05-26 22:49:19', 20700006, 1, 1),
(12, 5, 750, '', '2021-05-28 01:25:05', 20100010, 1, 2),
(13, 4, 600, '', '2021-05-28 01:25:05', 20100008, 1, 2),
(14, 1, 150, '', '2021-05-28 22:09:03', 20100002, 1, 3),
(15, 3, 450, '', '2021-05-28 22:09:25', 20100008, 1, 4),
(16, 1, 150, '', '2021-05-28 22:09:43', 20100008, 1, 5),
(17, 1, 150, '', '2021-05-28 22:10:04', 20100012, 1, 6),
(18, 3, 75, '', '2021-05-28 22:10:34', 20700026, 1, 7),
(19, 6, 900, '', '2021-05-28 22:11:08', 20100008, 1, 8),
(20, 2, 300, '', '2021-05-28 22:11:40', 20100012, 1, 9),
(21, 3, 75, '', '2021-05-28 22:12:15', 20700025, 1, 10),
(22, 15, 2250, '', '2021-06-01 20:32:34', 20100002, 3, 11),
(23, 1, 150, '', '2021-06-01 20:32:34', 20100005, 3, 11),
(24, 4, 600, '', '2021-06-01 20:32:35', 20100010, 3, 11),
(25, 1, 150, '', '2021-06-01 20:32:35', 20100016, 3, 11),
(26, 1, 150, '', '2021-06-01 20:32:35', 30100034, 3, 11),
(27, 1, 40, '', '2021-06-01 21:38:35', 20400001, 3, 12),
(28, 2, 50, '', '2021-06-04 17:39:36', 20700026, 1, 13),
(29, 3, 75, '', '2021-06-04 17:39:36', 20800004, 1, 13),
(30, 1, 200, '', '2021-06-04 17:39:37', 30200038, 1, 13),
(31, 5, 750, '', '2021-06-15 01:04:39', 20100001, 1, 14),
(32, 3, 450, '', '2021-06-15 18:51:41', 20100002, 1, 15),
(33, 2, 300, '', '2021-06-15 18:51:41', 20100006, 1, 15),
(34, 5, 750, '', '2021-06-15 19:45:02', 20100012, 3, 16),
(35, 1, 150, '', '2021-06-15 19:45:02', 20200002, 3, 16),
(36, 1, 150, '', '2021-06-15 19:45:03', 20100009, 3, 16);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`AccountID`);

--
-- Indexes for table `account_table`
--
ALTER TABLE `account_table`
  ADD PRIMARY KEY (`AccountID`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`CartID`);

--
-- Indexes for table `notif`
--
ALTER TABLE `notif`
  ADD PRIMARY KEY (`notifID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `product_temp`
--
ALTER TABLE `product_temp`
  ADD PRIMARY KEY (`Product_tempID`);

--
-- Indexes for table `restock`
--
ALTER TABLE `restock`
  ADD PRIMARY KEY (`RestockID`);

--
-- Indexes for table `restockdetail`
--
ALTER TABLE `restockdetail`
  ADD PRIMARY KEY (`restockDetailID`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`SalesID`);

--
-- Indexes for table `salesdetail`
--
ALTER TABLE `salesdetail`
  ADD PRIMARY KEY (`SalesDetailID`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`SupplierID`);

--
-- Indexes for table `supplier_temp`
--
ALTER TABLE `supplier_temp`
  ADD PRIMARY KEY (`SupplierID`);

--
-- Indexes for table `temprestock`
--
ALTER TABLE `temprestock`
  ADD PRIMARY KEY (`tempRestockID`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`TransactionID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `AccountID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `account_table`
--
ALTER TABLE `account_table`
  MODIFY `AccountID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `CartID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `notif`
--
ALTER TABLE `notif`
  MODIFY `notifID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30300047;

--
-- AUTO_INCREMENT for table `product_temp`
--
ALTER TABLE `product_temp`
  MODIFY `Product_tempID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `restock`
--
ALTER TABLE `restock`
  MODIFY `RestockID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `restockdetail`
--
ALTER TABLE `restockdetail`
  MODIFY `restockDetailID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `SalesID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `salesdetail`
--
ALTER TABLE `salesdetail`
  MODIFY `SalesDetailID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `SupplierID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40100017;

--
-- AUTO_INCREMENT for table `supplier_temp`
--
ALTER TABLE `supplier_temp`
  MODIFY `SupplierID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `temprestock`
--
ALTER TABLE `temprestock`
  MODIFY `tempRestockID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `TransactionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
