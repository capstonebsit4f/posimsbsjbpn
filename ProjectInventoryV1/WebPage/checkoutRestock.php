<?php include('session.php'); 
if ($_SESSION['AccountType'] != "AdminLevel"){
	session_destroy();
	echo'<script>alert("You don`t have necessary permission to access this page!"); window.location.href="LoginRegisterPage/login.php";</script>';
	
}

?>
<html>
 <!DOCTYPE html>
<html>
<head>
<title>Check-out</title>
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	 <script src="jquery-3.6.0.min.js"></script>


 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/transaction22v3.css" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
 </head>
 <body>

 <ul> 
	<center>
		
<div id="hovve" style="position:absolute; left:0%; top:0%; height:100%; width:20%;">  </div>
	<li class = "widthforli"> <a class="inputbuttoon"href="home.php"><img class="notimg"src="images/home1.png"><img class="hoverr"src="images/homes.png"> Home </a></li> 
	<!--<li></li><li></li><li></li>--->
    <li class = "widthforli"><a class="inputbuttoon" href="transaction_main.php"><img class="notimg"src="images/formOrder1.png"><img class="hoverr"src="images/formOrders.png">Transaction </a></li> 
    <!--<li></li><li></li><li></li>-->
    <li class = "widthforli" id="active"><a class="inputbuttoon"   id="active"   href="inventory.php"><img class="notimg"src="images/inventory1.png"><img class="hoverr"src="images/inventorys.png">Inventory </a></li> 

     <li class = "widthforli"><a class="inputbuttoon"  href="sales.php" ><img class="notimg"src="images/sale1.png"><img class="hoverr"src="images/sales.png">Sales </a></li> 
	<!--<li></li><li></li><li></li>-->
    <li class = "widthforli"><a class="inputbuttoon"  href="account_main.php"><img class="notimg"src="images/account1.png"><img class="hoverr"src="images/accounts.png">Account </a></li>
    <li class = "widthforli"><a class="inputbuttoon"  href="notification_new.php"><img class="notimg"src="images/notification1.png"><img class="hoverr"src="images/notifications.png">Notifications </a></li>
     <li class = "widthforli" id="cancelledd"><a class="cancelledd" >  </a></li>
    <p class="borderrr">     </p>
    <!--<li></li><li></li><li></li>-->

</center>

</ul>

<body>
<div id="rightPart">	
<div class="container">
	<br>

	<h1 align="center" style="font-family:arial; font-size:25px; font-weight:bold;">ORDER LIST</h1>

	<div style="height:10px;"></div>
	<div id="checkout_area"></div>

<br>
	<div style="height:20px;"></div>
	<div class="row" align="center">
		<button type="button" id="confirmCheck" style="margin-right:15px; background-color: rgb(64,46,68); color:white; height:40; width:200;"><i></i> Confirm Orders</button>


	</div>
</div>
</div>

<div id="alerrt"></div>
<script> 

$(document).ready(function(){
	showAddOrderRestock();
}); 

function showAddOrderRestock(){
	$.ajax({
		url:"addOrder_DataRestock.php",
		method:"POST",
		data:{
			check: 1,
		},
		success:function(response){
			$('#checkout_area').html(response);
		}
	});
}	

$(document).on('click', '.remove_prodRestock', function(){
		var ProductID=$(this).val();
		$.ajax({
			url:"remove_product_restock.php",
			method:"POST",
			data:{
				id: ProductID,
				rem: 1,
			},
			success:function(data){
				showAddOrderRestock();
			}
		});
	});
		
	$(document).on('click', '.minus_qty2Restock', function(){
		var ProductID=$(this).val();
		$.ajax({
			url:"Update_MinusQtyRestock.php",
			method:"POST",
			data:{
				id: ProductID,
				min: 1,
			},
			success:function(result){
				$("#alerrt").html(result);
				showAddOrderRestock();
			}
		});
	});
	
	$(document).on('click', '.add_qty2Restock', function(){
		var ProductID=$(this).val();
		$.ajax({
			url:"Update_AddQtyRestock.php",
			method:"POST",
			data:{
				id: ProductID,
				add: 1,
			},
			success:function(result){
				$("#alerrt").html(result);
				showAddOrderRestock();
			}
		});
	});

	$(document).on('click', '#confirmCheck', function(){
		var total=0;
		total=$('#totalP1').text();
		if(total == 0 || total ==0.00 || total <0 ||total==''){
			alert("Empty Cart!");
		}else{
		window.location.href='confirm_check_restock.php?total='+total;}
	});
</script>
</body>
</html>