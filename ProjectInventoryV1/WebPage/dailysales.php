<?php
$con  = mysqli_connect("localhost","root","","inventorydb");
 if (!$con) {
     # code...
    echo "Problem in database connection! Contact administrator!" . mysqli_error();
 }else{



       $sql ="

       SELECT calendar.datefield AS date, IFNULL(SUM(coalesce(sales.SalesTotal)),0) AS total_sales FROM sales RIGHT JOIN calendar ON (DATE(sales.SalesDate) = calendar.datefield) WHERE (calendar.datefield BETWEEN  Date_add(Now(),interval -7 day) AND Date(NOW()) ) GROUP BY date LIMIT 7" ;



       //- SELECT SalesDate, sum(coalesce(SalesTotal,0)) as total_sale
//FROM sales
//WHERE MONTH(SalesDate) = MONTH(CURRENT_DATE())
//AND YEAR(SalesDate) = YEAR(CURRENT_DATE()) group by SalesDate";


        $result = mysqli_query($con,$sql);
         $chart_data="";
         while ($row = mysqli_fetch_array($result)) { 
 
            $day[]  = $row['date'] ;
            $sales[] = $row['total_sales'];
        }
 
 }
 
?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sales</title> 
    </head>
    <style>
        #grad1 {
  height: 200px;
 /* For browsers that do not support gradients */
  background-image: linear-gradient(to top left, white, salmon);
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);

}

    </style>
    <body>
         
         <div style="width:45%;height:85%;text-align:center; top:3%; left:21%;  border:5px solid violet; position: absolute; border-color:#402e44; " id="grad1">
            <h2 class="page-header" >Weekly Sales Report</h2>
            <div>
 <?php
//$today = date("F d, Y");
//$lastWeek = date("F d, Y", strtotime("-7 days"));
//echo $today; echo " to "; 
//echo $lastWeek;
?>
<br>
            </div>
             <canvas  id="chartjs_bar"></canvas> 
            
        </div> 
      
    </body>
     
  <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">
      var ctx = document.getElementById("chartjs_bar").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels:<?php echo json_encode($day); ?>,

                        datasets: [{
                         
                            backgroundColor: [
                               "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7"
                                
                               
                                
                            ],
                            data:<?php echo json_encode($sales); ?>,
                        }]
                    },
                    options: {
                           legend: {
                        display: true,
                        position: 'hidden',
 
                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,


                        }
                    },
 
 
                }
                });
    </script>

</html>