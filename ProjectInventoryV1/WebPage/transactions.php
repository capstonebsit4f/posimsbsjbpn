<?php
include("session.php");
if ($_SESSION['AccountType'] != "AdminLevel"){
	session_destroy();
	echo'<script>alert("You don`t have necessary permission to access this page!"); window.location.href="LoginRegisterPage/login.php";</script>';	
}




$sqlGetNotifVal=mysqli_query($conn,"SELECT notifValue as notifVal, notifBool as notifBoolean from `notif` WHERE notifID='1'");
	$rowNotif = mysqli_fetch_assoc($sqlGetNotifVal);
	$notificationValue = $rowNotif["notifVal"];
	$notificationBool = $rowNotif["notifBoolean"];

?>

<!DOCTYPE html>
<html>
<head>
<title>Transaction Page</title>
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	<script src="jquery-3.6.0.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  	<link rel="stylesheet" href="css/transaction22v2.css" />

<!-- style for notif -->
  	<style>
 .content1 {
        background-color:#402e44;
        left: 24%;
      position: sticky;
        top: 8%;
        width: 1000px;
        height: 550px;
        z-index:9999;
        box-shadow:  0 0 80px rgba(255, 255, 255, 0.9);

    }
 .content {
        margin-left: 5px;
        margin-top: 40px;
        width: 990px; 
        height:505px; 
        background-color: #c790b9;
        position: fixed;
    }
    .pnq {
         width: 600px; 
        height:475px;
        background-color:#402e44;
        margin-top: 15px;  
        margin-left: 15px;
        position: fixed;

    }
    .pnq1 {
        background-color: white;
        margin-left: 5px;
        margin-top: 40px;
        height: 430px;
        width: 590px;
    }

    .white {
        background-color: white;
        margin-left: 610px;
        width: 338px;
        height: 350px;
        margin-top: -465px;
        position:fixed;
        border: 4px solid #402e44;
    }
    .button1 {
        
        width: 27%;
        height: 20%;
        background-color: red;
     	bottom:0%;
     	right:-29%;
        position: absolute;
        font-family: arial; color:white;
        font-size:15px;
        font-weight:bold;
    }
    .button2 {
        
        width: 27%;
        height: 20%;
        background-color: green;
        bottom:0%;
        right:-60%;
        position: absolute;
        font-family: arial; color:white;
        font-size:15px;
        font-weight:bold;

}

div #dataAlert::-webkit-scrollbar {
  width: 5px !important;
    border-radius: 10px;
}

/* Track */
div #dataAlert::-webkit-scrollbar-track {
  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
  background-color:white;
}
div #dataAlert/*thumb*/ 
::-webkit-scrollbar-thumb {
  background-color: rgb(199,144,185);
  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
  margin:2px;
  border-radius: 20px;
}

/* Handle on hover */
div #dataAlert::-webkit-scrollbar-thumb:hover {
  background: #999;
  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
}

  </style>
</head>
<body>

<ul> 
<center>		
<div id="hovve" style="position:absolute; left:0%; top:0%; height:100%; width:20%;">  </div>
	<li class = "widthforli"> <a class="inputbuttoon"href="home.php"><img class="notimg"src="images/home1.png"><img class="hoverr"src="images/homes.png"> Home </a></li> 
    <li class = "widthforli" id="active"><a class="inputbuttoon"  id="active"  href="transaction_main.php"><img class="notimg"src="images/formOrder1.png"><img class="hoverr"src="images/formOrders.png">Transaction </a></li> 
    <li class = "widthforli"><a class="inputbuttoon"  href="inventory.php"><img class="notimg"src="images/inventory1.png"><img class="hoverr"src="images/inventorys.png">Inventory </a></li> 
     <li class = "widthforli"><a class="inputbuttoon"  href="sales.php" ><img class="notimg"src="images/sale1.png"><img class="hoverr"src="images/sales.png">Sales </a></li> 
    <li class = "widthforli"><a class="inputbuttoon"  href="account_main.php"><img class="notimg"src="images/account1.png"><img class="hoverr"src="images/accounts.png">Account </a></li>
    <li class = "widthforli"><a class="inputbuttoon"  href="notification_new.php"><img class="notimg"src="images/notification1.png"><img class="hoverr"src="images/notifications.png">Notifications </a></li>
     <li class = "widthforli" id="cancelledd"><a class="cancelledd" >  </a></li>
    <p class="borderrr">     </p>
</center>
</ul>



<div id="CartTransaction" style="position:absolute; right:0%; top:0%; height:100%; width:26.5%; background-color:white;">   

	<div id="topDetails"  style="position:absolute; right:10%; top:0%; height:30%; width:80%; background-color:white;"> 
		<P style="font-family:monospace;font-size:14px; padding-top:10px; font-weight:bold; text-align:center">JIMLYN'S BALLOON AND PARTY NEEDS</P><HR/>
		<table style=" width:110%; margin-left:0% !important; margin-top:-1% !important; margin-bottom:-3% !important;  padding:0; background-color:white; border:none">
			<tr>	
				<td style="border:none;font-family:monospace;font-size:10px; padding:0px; font-weight:bold; text-align:left;">Processed By: <span id="usernamee"><?php echo $_SESSION['username']; ?></span> </td>
				<td style="border:none;font-family:monospace;font-size:10px; padding:0px; font-weight:bold; text-align:right;">Temp_Transaction ID: </td>
			</tr>
			<tr> 
				<td style="border:none;font-family:monospace;font-size:10px; padding:0px; font-weight:bold; text-align:left;">Date: <span id="timeDate"></span></td> 
			</tr>
		</table><HR/>
	</div>
<center>

<div class="dataTable" style="position:absolute; left: -2.5%; top: 12.5%; height:62% !important; width:100%; ">
<table style="width:95% !important; padding-right:20px; border:none !important;">

			<tr style="border:none !important; padding:0px !important; top:0% !important; right:0% !important;position:sticky !important; width:50% !important;"> 
				<th style="position:sticky !important; border:none !important;width: 18% !important; padding:0px !important;font-size:11px;">Product ID</th>
				<th style="position:sticky !important; border:none !important;width: 42% !important;padding:0px !important; font-size:11px; ">Name</th>
				<th style="position:sticky !important; border:none !important;width: 20% !important;padding:0px !important; font-size:11px;">Qty</th>
				<th style="position:sticky !important; border:none !important;width: 20% !important;padding:0px !important; font-size:11px;">Price</th>
			</tr>
<?php
?>

</table> 
	<div id="tableCart" style="width:95% !important; height:94% ;padding-right:0px; padding-top:0px; border:none !important; overflow:scroll; overflow-x:hidden; position:absolute; top:5%; right:0%;"> </div></div>
	<div id="tableeCart" style=""> </div>
	<div id="button2s" style="position:absolute; right:10%; text-align:center;bottom:20%;width:80% !important; height:6%;">
		<hr/>
		<p style="padding-left:45%;text-align:left; font-size:15px; font-weight:bold;font-family:monospace;" >Total:  <span id="totalP"> </span>  PHP</p>
	</div>
	</center>

	<div id="button2s" style="position:absolute; bottom:0%;width:100% !important; height:14%;">
		<button type="reset" id="confirmButton" onclick="showConfirm()"> CHECK-OUT </button>
		<button type="reset" id="cancelButton" onclick="showCancel()"> CANCEL </button>
	</div>
</div>

<div id="rightPart">		
  <div class="container" >
   <div class="form-group">
    <div class="input-group" style="background-color:#333;width:45%;height:45px !important;margin-left:6px !important;">
    	<input type="text" name="search_text" id="search_text" autocomplete="off" placeholder="Search the products..." required oninvalid="this.setCustomValidity('Please enter valid text')" oninput="setCustomValidity('')"  style="margin: 7px !important;margin-left:15px !important;height:25px;width:87%;font-size:13px;margin-bottom:20px; font-family:monospace"/>
        <img src="images/search1.png" style="margin-bottom: -5px;height:20px; width:20px;"  />
    </div>
 </div>
   <br /><br/><br/>

   <div style="height:50% !important; position:absolute; margin-top:-15px !important; background-color: rgb(64,46,68) !important; left:0.75%; width:65% !important;"><br></div>
   <div style="height:50% !important; position:fixed; margin-bottom:-15px !important; background-color: rgb(64,46,68) !important; left:21.1%; bottom: 5% !important; width:51.3% !important;"><br></div>
   <div id="result"style="height:400px !important; background-color: rgb(64,46,68) !important;overflow: scroll; left:0.75% !important;position:absolute;  overflow-x:hidden; width:65% !important; scroll-padding: 50px !important;
"></div>

<div id="bottomSelectedProduct" style="position:fixed; left: 22.4% ; bottom:5%; height:110px !important; width:48.75%; background-color:white;" >	</div>
</div>
	<div id="PreventClick" style="position:fixed; top:0%; left:0%; height:100%; width:100%; display:none;" > 	
	   <div id="CancelOrder" style="display:inherit; position:fixed; left:40%; top:30%; height:30%; width:25%; background-color: white; border:2px solid black;   box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.7); " > 
		<center><p style="position:absolute; top:32%;left:0%; text-align:center; width:100%; font-family:arial; font-size:13.5px;"> Are you sure you want to cancel transaction? <br> <br>This action will empty your Cart</p></center>
		<button id="redButton" onclick="cancelShowCancel()">NO</button>
		<button id="greenButton" onclick="cancelCheckOut()">YES</button>
	</div>
</div>
 
<div id="PreventClickAdd" style="position:fixed; top:0%; left:0%; height:100%; width:100%; display:none;" > 	
   <div id="GoToOrder" style="display:inherit; position:fixed; left:40%; top:30%; height:30%; width:25%; background-color: white; border:2px solid black;   box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.7); " > 
   		<center><p style="position:absolute; top:32%;left:0%; text-align:center; width:100%; font-family:arial; font-size:13.5px;">Go to Check-out?</p></center>
		<button id="redButton" onclick="cancelShowConfirm()">NO</button>
		<button id="greenButton" onclick="goToCheckOut()">YES</button>
	</div>
</div> 

<div id="emptyCartAlert"></div> 






<!--for notif-->
<div style="position:fixed; height:100%; width:79.9%; left:20%; top:0%; z-index:999; display:none;" id="AlertBox">
<div style="position:absolute; height:100%; width:90%; left:3.9%; top:10%;">
<div class="content1">
    <h3 style="position: fixed; margin-left: 380px; margin-top: 9px; font-family: arial; color: white;">PRODUCT LOW-STOCK ALERT </h3>
<div class="content">
    <div class="pnq">
        <h4 style="position: fixed; margin-left:120px; font-family: arial; color: white; margin-top: 12px;">PRODUCT NAME</h4>
        <h4 style="position: fixed; margin-left:440px; font-family: arial; color: white; margin-top: 12px;">QUANTITY</h4>

        
        <div class="pnq1">
       
	<div id="dataAlert" style="overflow:scroll; overflow-x:hidden; height:99%; left:-2%; top:0.5%; width:102%; position:relative; font-size:14px !important;"></div>
    <div class="white">
        <p style=" position: fixed;margin-left: 55px; color: black; margin-top: 35px; font-size:15px; font-weight:600; font-family: arial;">

        	
        	&nbsp;&nbsp;&nbsp;The remaining quantity of the<br>&nbsp;following products is lower than<br>the minimum quantity that you've<br>&nbsp;&nbsp;&nbsp;set in the notification settings.

        	<br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is a reminder that the<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;following products must be<br>&nbsp;&nbsp;&nbsp;&nbsp;included in your next restock<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;schedule.

        	<br><br><br>To stop receiving this alert, go to<br>&nbsp;the notification tab and click the<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stop receiving alert button.

        	<br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thank you.

     	</p>
	 </div>
	</div>
 	
 	<div>

        <div>
           <button class="button1" type="button" onclick="hideAlerts()"> CLOSE</button>
        </div>    

        <div >
           <button class="button2" type="button" onclick="goToAlerts()" > &nbsp;&nbsp;GO TO <br>RESTOCK</button> 
        </div>
	</div>
</div>
</div>
</div>
</div>
</div>
<!--End of notif -->


</body>
</html>


<script>
function clicked(){
$(document).on("click", "#selectButton", function(){ /* WHEN A BUTTON IS CLICKED */
  var idRow = $(this).val(); /* GET VALUE */
  $.ajax({ 
    type: "GET", /* METHOD TO USE TO PASS THE DATA */
    url: "selectProduct.php", /* URL THAT PROCESS */
    data: { "idRow": idRow }, /* DATA TO BE PASSED ON */
    success: function(result){ /* GET THE RETURNED DATA */
      $("#bottomSelectedProduct").html(result); /* PUT THE RETURNED DATA DIV */
    }
  });
});}
</script>
<script>
$(document).ready(function(){
 load_data();
 load_cart();
 load_alert_data();
 showAlerts();

 function load_cart(queri)
 {
  $.ajax({
   url:"cart_data.php",
   method:"POST",
   data:{queri:queri},
   success:function(data)
   {
    $('#tableCart').html(data);
   }
  });
 }

 function load_data(query)
 {
  $.ajax({
   url:"fetch.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }

function load_alert_data(queryy)
 {
  $.ajax({
   url:"fetchProductsForRestock.php",
   method:"POST",
   data:{queryy:queryy},
   success:function(data)
   {
    $('#dataAlert').html(data);
   }
  });
 } 
 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });

 

});


var dbNotifValue = <?php echo $notificationBool; ?> ;
function showAlerts(){
	var alertBox = document.getElementById('AlertBox');
	
	if (dbNotifValue == '1'){
	alertBox.style.display = 'block'; 
	}else{
	alertBox.style.display = 'none'; 
	}
}

function hideAlerts(){
	var alertBox = document.getElementById('AlertBox');
	
	if (alertBox.style.display = 'block'){
	alertBox.style.display =  'none'; 
	}else{
	alertBox.style.display = 'block';
	}
}

function goToAlerts()	{
		window.location.href = "restock.php";
	}




	function getDateTime() {
        var now     = new Date(); 
        var year    = now.getFullYear();
        var month   = now.getMonth()+1; 
        var day     = now.getDate();
        var hour    = now.getHours();
        var minute  = now.getMinutes();
        var second  = now.getSeconds(); 
        if(month.toString().length == 1) {
             month = '0'+month;
        }
        if(day.toString().length == 1) {
             day = '0'+day;
        }   
        if(hour.toString().length == 1) {
             hour = '0'+hour;
        }
        if(minute.toString().length == 1) {
             minute = '0'+minute;
        }
        if(second.toString().length == 1) {
             second = '0'+second;
        }   
        var dateTime = month+'/'+day+'/'+year+' '+hour+':'+minute+':'+second;   
         return dateTime;
    }

    setInterval(function(){
        currentTime = getDateTime();
        document.getElementById("timeDate").innerHTML = currentTime;
    }, 1000);	

    function showCancel(){
    	var cancel = document.getElementById('PreventClick');
		if (cancel.style.display === 'none'){
		cancel.style.display = 'block'; 
		}
		}

    function cancelShowCancel(){
    	var cancel = document.getElementById('PreventClick');
		if (cancel.style.display === 'block'){
		cancel.style.display = 'none'; 
		}
		}

	function showConfirm(){
    	var confirm = document.getElementById('PreventClickAdd');
		if (confirm.style.display === 'none'){
		confirm.style.display = 'block'; 
		}
		}

    function cancelShowConfirm(){
    	var confirm = document.getElementById('PreventClickAdd');
		if (confirm.style.display === 'block'){
		confirm.style.display = 'none'; 
		}
		}	

	function goToCheckOut()	{
		window.location.href = "checkout.php";
	}

	function cancelCheckOut()	{
		$.ajax({
			url:"EmptyCart.php",
			method:"POST",
			data:{
				delete:1,
			},
			success:function(result){
				$("#emptyCartAlert").html(result);
				window.location.href = "transaction_main.php";
				}
		});
			
	}




</script>