<?php

include("session.php");

  /* ESTABLISH YOUR CONNECTION */
 $connectss = new mysqli("localhost","root", "", "inventorydb");
 	
  if($stmt = $connectss->prepare("SELECT `ProductID`,`ProductName`, `ProductDescription`, `ProductPrice`, `ProductQuantity`  FROM `products` WHERE `ProductID` = ?")){ /* PREPARE THE STATEMENT */
    $stmt->bind_param("s", $_GET["idRow"]); /* BIND THE PASSED ON DATA TO THE SELECT QUERY */
    $stmt->execute(); /* EXECUTE THE QUERY */
    $stmt->bind_result($ProductID,$ProductName, $ProductDescription, $ProductPrice, $ProductQuantity); /* BIND THE RESULT TO THESE VARIABLES */
    $stmt->fetch(); /* FETCH THE RESULTS */
    $total = 0;
							

    /* WHAT YOU ECHO IS WHAT WILL BE RETURNED TO THE AJAX REQUEST */
    echo '<div id="bottomHidden"  style="position:absolute; left:3%; top:0% !important; width:78% !important; height: 100% !important;background-color: white; "> 

					<div id="hiddenSelectedItem" style="height: 86%; width: 96.5%; left:-2%; top:5%; position:absolute; border:2px solid black; ">
					
					<p style="text-align:left; padding-left:2%;padding-bottom:1.5%;padding-top:1%; font-size:17px; font-family:monospace; font-weight:bold;">SELECTED PRODUCTS</p>
					<table style="width:95% !important; left:0%; position:absolute; border:0px !important;">

						<tr style="border:0px !important; text-align:left;">
							<td style="font-weight:bold;padding:0px; padding-bottom: 0px !important;width:20% !important; border-left:0px !important; border-right:0px !important; ">ProductID</td>
							<td style=" font-weight:bold;padding:0px; padding-bottom: px !important;text-align:center; width:60% !important; border-left:0px !important; border-right:0px !important; ">Product Name</td>
							<td style="font-weight:bold;padding:0px; padding-bottom: 0px !important;width:15% !important; border-left:0px !important; border-right:0px !important; ">Price</td>
						</tr>

						<tr style="border:0px !important; text-align:left;">
							<td style="padding:0px; width:20% !important; border-left:0px !important; border-right:0px !important; ">'.$ProductID.'</td>
							<td style=" padding:0px; text-align:center; width:60% !important; border-left:0px !important; border-right:0px !important; ">'.$ProductName.'</td>
							<td style="padding:0px; width:15% !important; border-left:0px !important; border-right:0px !important; ">'.$ProductPrice.'</td>
						</tr>


					</table>
					<div style="right:30%; bottom:2%; position:absolute; font-size:13px; font-weight:bold; font-family:monospace;">Total:  </div>
					<div style="right:12%; bottom:2%; position:absolute; font-size:13px; font-weight:bold; font-family:monospace;" id="totalDiv"></div>
				</div>
				 </div>

				 <div id="quantityADDCart"style="position:absolute; right:1.75%; top:5% !important; width: 19% !important; height:90% !important; overflow:hidden;">

				 	<p  style=" font-size:10px; font-weight:bold; font-family:monospace; padding-bottom:1% !important; padding-top:1% !important; color:black;text-align:left;">QUANTITY</p>
				 	<div >

						<input type="button" class="add" value="+" style=" font-size:16px; padding-bottom:17% !important; padding-top:1% !important; position: absolute; width:21% !important; left:0% !important; height:15px !important; top:13.75% !important; background-color:rgb(64,46,68); color:white;"/>  		
						<input type="text" id="quantity" value="0" style="position: absolute; top:13.75% ; right:24.5%; width:52% !important; height:28px !important; "/>

						<input type="button" class="sub" value="-" style=" font-size:16px; padding-bottom:17% !important;position: absolute; width:21% !important; right:0% !important; height:15px !important; top:13.75% !important; background-color:rgb(64,46,68); color:white;"/> 
				</div>

				<button type="button" name="add_to_cart" id="add_to_cart" class="add_to_cart" value="'.$ProductID.'" style="font-weight:bold; position: absolute; width:100% !important; right:0% !important; height:50px !important; bottom:3.75% !important; background-color:rgb(64,46,68); color:white;" >ADD TO CART</button> 
				</div>
			</div>

';
$stmt->close();
  } /* END OF PREPARED STATEMENT */
else{
	echo'errorrr';
}


?>

<script>

var total =0;
var subtotal = 0;
var totals = 0;

// if user changes value in field
$('#quantity').change(function() {
  // maybe update the total here?
  subtotal = document.getElementById("quantity").value * '<?php echo $ProductPrice?>';
  	totals = subtotal;
  	document.getElementById("totalDiv").innerHTML=totals + " Php";
}).trigger('change');

$('.add').click(function() {
  var target = $('#quantity', this.parentNode)[0];
  target.value = +target.value + 1;

  	subtotal = document.getElementById("quantity").value * '<?php echo $ProductPrice?>';
  	totals = subtotal;
  	document.getElementById("totalDiv").innerHTML=totals + " Php";
  
});

$('.sub').click(function() {
  var target = $('#quantity', this.parentNode)[0];
  if (target.value > 0) {
    target.value = +target.value - 1;

    subtotal =document.getElementById("quantity").value * '<?php echo $ProductPrice?>';
  	totals = subtotal; 
  	document.getElementById("totalDiv").innerHTML=totals + " Php";
  }

 
});

document.getElementById("totalDiv").innerHTML=totals + " Php";

$(document).on('click', '.add_to_cart', function(){
	
		var pid=$(this).val();
		var quanty=$('#quantity').val();
		var price = $('#quantity').val() * '<?php echo $ProductPrice ?>';
		
		if(quanty > 0 ){
			$.ajax({
				url:"add_cart.php",
				method:"POST",
				data:{
					pid: pid,
					quanty: quanty,
					price : price,
					
				},
				success:function(result){
					
					$("#tableCart").html(result);	
					$("#quantity").val() = 0;
					}
			});
			
		}
		else if(quanty=="" || quanty == 0 ){
			alert('Please enter Quantity');
			
		}
		else{
			alert('Please Enter a Numeric Value');
			
		}
	});
	

</script>