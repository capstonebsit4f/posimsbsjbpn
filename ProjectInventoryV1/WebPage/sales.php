<?php
require("session.php");
if ($_SESSION['AccountType'] != "AdminLevel"){
	session_destroy();
	echo'<script>alert("You don`t have necessary permission to access this page!"); window.location.href="LoginRegisterPage/login.php";</script>';
	
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Sales Page</title>
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/sapooples.css" />


<style> 
li a:hover, li:hover{
	 background-color: rgb(250,250,250) !important;
	 color:#333 !important;
}

</style>
</head>




<body>
<ul> 
	<center>
<div id="hovve" style="position:absolute; left:0%; top:0%; height:100%; width:20%;">  </div>
	<li class = "widthforli"> <a class="inputbuttoon" href="home.php"><img class="notimg"src="images/home1.png"><img class="hoverr"src="images/homes.png"> Home </a></li> 
    <li class = "widthforli"><a class="inputbuttoon"  href="transaction_main.php"><img class="notimg"src="images/formOrder1.png"><img class="hoverr"src="images/formOrders.png">Transaction </a></li> 
    <li class = "widthforli" ><a class="inputbuttoon"   href="inventory.php"><img class="notimg"src="images/inventory1.png"><img class="hoverr"src="images/inventorys.png">Inventory </a></li> 
     <li class = "widthforli" id="active"><a class="inputbuttoon"  href="sales.php"  id="active"><img class="notimg"src="images/sale1.png"><img class="hoverr"src="images/sales.png">Sales </a></li> 
    <li class = "widthforli"><a class="inputbuttoon"  href="account_main.php"><img class="notimg"src="images/account1.png"><img class="hoverr"src="images/accounts.png">Account </a></li>
    <li class = "widthforli"><a class="inputbuttoon"  href="notification_new.php"><img class="notimg"src="images/notification1.png"><img class="hoverr"src="images/notifications.png">Notifications </a></li>
     <li class = "widthforli" id="cancelledd"><a class="cancelledd" >  </a></li>
    <p class="borderrr">     </p>

</center>

</ul>



<div id="rightPart" style="height:57%;">	

<center>
<br>
<br>
<form action="" method="POST">	
<br>		

<div style="  position:fixed; bottom:2%; right:0.90%; background-color: rgb(64,46,68) !important;  width:78.2% !important; height:64%;"><br> </div>

	<div id="searchResult"	style="height:89% !important; background-color: rgb(64,46,68) !important;right:0%; width:98.9% !important; position:relative; ">

			<div class="row" style="height:385px !important; margin-top:16.5%; background-color:white;  width:96.6%; ">

                <div class="col-lg-12" style="height:90%; background-color:white; width:99.5%; overflow: scroll; overflow-x:hidden; scroll-padding: 50px !important; ">
					<table width="90%" class="table table-striped table-bordered table-hover" id="historyTable">
   						<tr style="position:sticky; left:0; top:0 !important;">

   						<th style="width:12%; text-align:center !important; font-size:13px !important;">Sales_ID</th>
   						<th style="width:12%; text-align:center !important; font-size:13px !important;">Account_ID</th>
   						<th style="width:10%; text-align:center !important; font-size:13px !important;">Quantity Sold</th>
   						<th style="width:15%; text-align:center !important; font-size:13px !important;">No. of Products Sold</th>
   						<th style="width:11%; text-align:center !important; font-size:13px !important;">Total</th>
   						<th style="width:15%; text-align:center !important; font-size:13px !important;">Date</th>
   						<th style="width:15%; text-align:center !important; font-size:13px !important;">Action</th>

   					</tr>



<?php
	$conn = mysqli_connect("localhost","root", "");
	$db = mysqli_select_db($conn, 'inventorydb');
	

	$quary = "SELECT * FROM `sales`";
    	$queryResult = mysqli_query($conn, $quary);


    	
    	if ($queryResult->num_rows >0){
		while($row = $queryResult->fetch_assoc()){

			?>

			<tr>
							<td class="hidden"></td>
							<td><?php echo $row['SalesID']; ?></td>
							<td><?php echo $row['AccountID']; ?></td>
							<td><?php echo $row['SalesTotalQty']; ?></td>
							<td><?php echo $row['SalesCountProduct']; ?></td>
							<td align="right"><?php echo number_format($row['SalesTotal'],2); ?></td>
							<td><?php echo date('M d, Y h:i A',strtotime($row['SalesDate'])); ?></td>

							

							<td>
								<a href="#detail<?php echo $row['SalesID']; ?>" data-toggle="modal" class="btn btn-primary btn-sm" style="background-color:#333 !important; color:white; padding:4px; border:none; padding-left:23%; padding-right:23%;">View Details</a>
								<?php include ('fullDetails.php'); ?>
							</td>
						</tr>
					<?php
					}

		}
	?>
   				</form></center> <br> </div> </div></div>

   			<?php
   

?>


</div>

</div>





</div>
<?php
	$quaryyy = mysqli_query($conn, "SELECT SUM(SalesTotalQty) AS sumCountPrdct FROM `sales`");
    	$rowsCount = mysqli_fetch_assoc($quaryyy);
		$sumCount = $rowsCount["sumCountPrdct"];

		//most Popular
		$quaryMost = mysqli_query($conn, "SELECT ProductID, SUM(Sales_qty) AS sumQtyPrdct FROM `salesdetail` GROUP BY ProductID ORDER BY sumQtyPrdct desc LIMIT 1");
    	$rowsQtyP = mysqli_fetch_assoc($quaryMost);
		$sumQtyP = $rowsQtyP["ProductID"];
		//leastPopular
		$quaryLeast = mysqli_query($conn, "SELECT ProductID, SUM(Sales_qty) AS sumQtyPrdctL FROM `salesdetail` GROUP BY ProductID ORDER BY sumQtyPrdctL asc LIMIT 1");
    	$rowsQtyPL = mysqli_fetch_assoc($quaryLeast);
		$sumQtyPL = $rowsQtyPL["ProductID"];

		//profitTrial

		$quaryProfit = mysqli_query($conn, "SELECT SUM(ProductPrice*Sales_qty)-SUM(ProductSupplierCost*Sales_qty) AS sumProfit FROM `salesdetail` INNER JOIN(SELECT ProductSupplierCost, ProductID, ProductPrice FROM products) AS productJoin ON salesdetail.ProductID = productJoin.ProductID;");
    	$rowsQtyProfit = mysqli_fetch_assoc($quaryProfit);
		$sumProfits = $rowsQtyProfit["sumProfit"];

		$quarySales = mysqli_query($conn, "SELECT SUM(ProductPrice*Sales_qty) AS sumSales FROM `salesdetail` INNER JOIN(SELECT ProductID, ProductPrice FROM products) AS productJoin ON salesdetail.ProductID = productJoin.ProductID;");
    	$rowsQtySales = mysqli_fetch_assoc($quarySales);
		$sumSaless = $rowsQtySales["sumSales"];

		//mostPopular
		$quaryMostP = mysqli_query($conn, "SELECT ProductName FROM `products` WHERE ProductID = '".$sumQtyP."'");
    	$rowsProductMost = mysqli_fetch_assoc($quaryMostP);
		$MostPopular = $rowsProductMost["ProductName"];

		//leastPopular
		$quaryLeastP = mysqli_query($conn, "SELECT ProductName FROM `products` WHERE ProductID = '".$sumQtyPL."'");
    	$rowsProductLeast = mysqli_fetch_assoc($quaryLeastP);
		$LeastPopular = $rowsProductLeast["ProductName"];
?>

<div id="bottomButtons" style='position:fixed; right:1.25% !important; width: 77.5%; height: 29.5%;top:2%; font-size:13px; font-family:monospace; background-color:rgb(175,100,156)'> 

<button id="btns1" style="font-size:16px !important;font-weight:bold; font-family:monospace; background-color: rgb(251, 250, 254); border:none; color:rgb(64,46,69);  width:22%; position:absolute; left:1.5%; bottom:11.5%;  height:76%;" type="button" onclick="window.location.href='sales_graph.php'"><img id="graph1"  src="images/grph1.png" style="height:125px; width:195px;"/><img id="graph2" src="images/grph.png" style="height:125px; width:195px;"/><span style="position:absolute; bottom:2%; left:0%; right:0%; text-align:center; width:100%;"> Graphs</span></button>

<button id="btns2" style="font-size:140% !important;font-weight:bold; font-family:monospace; background-color: rgb(255,186,184); border:none; color:white; width:36%; position:absolute; left:25.5%; top:11.5%;  height:35%; "><span style="font-size:16px !important; color:rgb(64,46,69); position:absolute; bottom:2%; left:0%; right:0%; text-align:center; width:100%;">Most Popular Product</span><?php echo $MostPopular ?></button>

<button id="btns2" style="font-size:140% !important;font-weight:bold; font-family:monospace; background-color: rgb(255,186,184); border:none; color:white; width:36%; position:absolute; right:1.5%; top:11.5%;  height:35%; "><span style="font-size:16px !important; color:rgb(64,46,69); position:absolute; bottom:2%; left:0%; right:0%; text-align:center; width:100%;">Least Popular Product</span><?php echo $LeastPopular ?></button>

<button id="btns3" style="font-size:140% !important;font-weight:bold; font-family:monospace; background-color: rgb( 255, 249, 197 ); border:none; color:rgb(194,46,69);  width:23%; position:absolute; right:51.5%; bottom:11.5%;  height:35%; "><span style="font-size:16px !important; color:rgb(64,46,69); position:absolute; bottom:2%; left:0%; right:0%; text-align:center; width:100%;">Total Sales</span> <?php echo $sumSaless ?></button>

<button id="btns3" style="font-size:140% !important;font-weight:bold; font-family:monospace; background-color: rgb( 255, 249, 197 ); border:none; color:rgb(194,46,69);  width:23%; position:absolute; right:26.5%; bottom:11.5%;  height:35%; "><span style="font-size:16px !important; color:rgb(64,46,69); position:absolute; bottom:2%; left:0%; right:0%; text-align:center; width:100%;">Total Products Sold</span> <?php echo $sumCount ?></button>

<button id="btns3" style="font-size:140% !important;font-weight:bold; font-family:monospace; background-color: rgb( 255, 249, 197 ); border:none; color:rgb(194,46,69);  width:23%; position:absolute; right:1.5%; bottom:11.5%;  height:35%; "><span style="font-size:16px !important; color:rgb(64,46,69); position:absolute; bottom:2%; left:0%; right:0%; text-align:center; width:100%;">Total Profit</span> <?php echo $sumProfits ?></button>

</div>




</div> 

</body>
</html>

