 
<?php
$con  = mysqli_connect("localhost","root","","inventorydb");
 if (!$con) {
     # code...
    echo "Problem in database connection! Contact administrator!" . mysqli_error();
 }else{
         $sql = "SELECT Concat('Week ', weekofyear(calendar.datefield)) as date, calendar.datefield as datee, IFNULL(SUM(coalesce(sales.SalesTotal)),0) AS total_sales FROM sales RIGHT JOIN calendar ON (DATE(sales.SalesDate) = calendar.datefield) WHERE (month(calendar.datefield) = month(NOW()) and year(calendar.datefield) = year(NOW())) GROUP BY date order by datefield asc";



      
         $result = mysqli_query($con,$sql);
         $chart_data="";
         while ($row = mysqli_fetch_array($result)) { 
 
            $month[]  = $row['date'];
            $total_sales[] = $row['total_sales'];


        }
 
 
 }
 
 
 
?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sales</title> 
    </head>
    <style>
        #grad2 {
  height: 200px;
  background-color: red; /* For browsers that do not support gradients */
  background-image: linear-gradient(to bottom right, Orchid, white);
}

    </style>
    <body>
       
        <div style="width:31%; height:45%;text-align:center;  right:1%; top:3%; border:5px solid violet; position: absolute; border-color:#402e44;" id="grad2">
            <h2 class="page-header" style="font-size: 15px;" >Monthly Sales Report</h2>
            <?php
echo date('F, Y');
?>
           
            
            <canvas  id="chartjs_bar2">
                
            </canvas> 
         
        </div>
  
    </body>
  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">
      var ctx = document.getElementById("chartjs_bar2").getContext('2d');
      			var week = "Week";
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels:  <?php echo json_encode($month); ?>,
                        datasets: [{
                            backgroundColor: [
                                "#AA00D7", 
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7"
                                
                            ],
                            data:<?php echo json_encode($total_sales); ?>,
                        }]
                    },
                    options: {
                           legend: {
                        display: true,
                        position: 'hidden',
 
                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,
                        }
                    },
 
 
                }
                });
    </script>
</html>