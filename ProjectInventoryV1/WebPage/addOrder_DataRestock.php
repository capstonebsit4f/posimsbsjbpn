<?php
	include('session.php');
	if(isset($_POST['check'])){
		?>

	
		<table width="100%" style="border:none !important;" class="noBorderCheckOutTbl">
			<thead>
				<th style="width:10%;">Action</th>
				<th style="width:38%;">Product Name</th>
				<th style="width:8%;">In Stock Qty</th>
				<th style="width:7%;">Price</th>
				<th style="width:7%;">Unit Cost</th>
				<th style="width:5%;">UnDMGD Qty</th>
				<th style="width:15%;">Added Qty</th>
				<th style="width:8%;">SubTotal</th>
			</thead>
			<tbody>
			<form method="POST" action="">
			<?php
				$total=0;
				$query=mysqli_query($conn,"select * from tempRestock left join products on products.ProductID=tempRestock.ProductID where AccountID='".$_SESSION['id']."'");
				while($row=mysqli_fetch_array($query)){
					?>
					<tr>
						<td  style="overflow:hidden; width:10%;" ><button type="button" class="remove_prodRestock" value="<?php echo $row['ProductID']; ?>"><i></i> Remove</button></td>
						<td  style="overflow:hidden; width:38%;"><?php echo $row['ProductName']; ?></td>
						<td  style="overflow:hidden; width:8%;"><?php echo $row['ProductQuantity']; ?></td>
						<td  style="overflow:hidden; width:7%;" align="right"><?php echo number_format($row['ProductPrice'],2); ?></td>
						<td  style="overflow:hidden; width:7%;"><?php echo $row['UnitCost']; ?></td>
						<td  style="overflow:hidden; width:5%;"><?php echo $row['UndamagedQty']; ?></td>


						<td  style="overflow:hidden; width:15%; padding-left:20px;" align="left">
							<button type="button" class="minus_qty2Restock" value="<?php echo $row['ProductID']; ?>"style="font-size:14px; width:20% !important; height:10% !important;  background-color:rgb(64,46,68); color:white;"><i> - </i></button>

							<button type="button"class="add_qty2Restock" value="<?php echo $row['ProductID']; ?>"style="font-size:14px;  width:20% !important;  height:10% !important; background-color:rgb(64,46,68); color:white;"><i>+</i></button> 
							
							<span align="right" style="text-align:right; padding-left:15% !important;"><?php echo $row['qty'];?></span>
													</td>
						<td  style="overflow:hidden; width:8%;" align="right"><strong><span><?php echo $row['qtyPrice']?></span></strong></td>
					</tr>
					<?php

					
					$sumQuery = 	mysqli_query($conn,"select SUM(qtyPrice) AS sum_value from `tempRestock` where AccountID='".$_SESSION['id']."'");
					$rowss = mysqli_fetch_assoc($sumQuery);
					$sum = $rowss["sum_value"];
					if($sum === "0"){
						$sum = 0;
					}

				}
			?>
			<tr class="bottomCheckOutTbl">
				<td colspan="7" align="right"><span style="font-weight:bold;">Grand Total</span></td>
				<td align="right"><strong><span id="totalP1">  </span><strong></td>
			</tr>
			</form>
			</tbody>
		</table>

		<?php
	}
?>

<script> 

	$("#totalP1").text("<?php echo number_format($sum,2); ?>"); 

</script>