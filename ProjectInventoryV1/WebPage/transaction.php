<?php
?>
<!DOCTYPE html>
<html>
<head>
<title>Transaction Page</title>
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	 <script src="jquery-3.6.0.min.js"></script>


 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/transactionv2.css" />


<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
$(document).ready(function(){
    $('.search-box input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings("#searchResult");
        if(inputVal.length){
            $.get("backend.php", {term: inputVal}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", "#searchResult p", function(){
        $(this).parents(".search-box").find('input[type="text"]').val($(this).text());
        $(this).parent("#searchResult").empty();
    });
});
</script>
</head>
<body>

<ul> 
	<center>
		
	
	<!--<li></li><li></li><li></li>-->
	<div id="hovve">  </div>
	<li class = "widthforli"> <a class="inputbuttoon"href="home.php"><img class="notimg"src="images/home1.png"><img class="hoverr"src="images/homes.png"> Home </a></li> 
	<!--<li></li><li></li><li></li>--->
    <li class = "widthforli" id="active"><a class="inputbuttoon"  id="active"  href="transaction.php"><img class="notimg"src="images/formOrder1.png"><img class="hoverr"src="images/formOrders.png">Transaction </a></li> 
    <!--<li></li><li></li><li></li>-->
    <li class = "widthforli"><a class="inputbuttoon"  href="inventory.php"><img class="notimg"src="images/inventory1.png"><img class="hoverr"src="images/inventorys.png">Inventory </a></li> 

     <li class = "widthforli"><a class="inputbuttoon"  href="sales.php" ><img class="notimg"src="images/sale1.png"><img class="hoverr"src="images/sales.png">Sales </a></li> 
	<!--<li></li><li></li><li></li>-->
    <li class = "widthforli"><a class="inputbuttoon"  href="account_main.php"><img class="notimg"src="images/account1.png"><img class="hoverr"src="images/accounts.png">Account </a></li>
    <li class = "widthforli"><a class="inputbuttoon"  href="notification.php"><img class="notimg"src="images/notification1.png"><img class="hoverr"src="images/notifications.png">Notifications </a></li>
     <li class = "widthforli" id="cancelledd"><a class="cancelledd" >  </a></li>
    <p class="borderrr">     </p>
    <!--<li></li><li></li><li></li>-->

</center>

</ul>


<div id="CartTransaction" style="position:absolute; right:0%; top:0%; height:100%; width:26.5%; background-color:white;">   

	<div id="topDetails"  style="position:absolute; right:10%; top:0%; height:30%; width:80%; background-color:white;"> 

		<P style="font-family:monospace;font-size:14px; padding-top:10px; font-weight:bold; text-align:center">JIMLYN'S BALLOON AND PARTY NEEDS</P>
		<HR/>
		
		<table style=" width:110%; margin-left:0% !important; margin-top:-1% !important; margin-bottom:-3% !important;  padding:0; background-color:white; border:none">
			<tr>	
				<td style="border:none;font-family:monospace;font-size:10px; padding:0px; font-weight:bold; text-align:left;">Processed By: </td>
				<td style="border:none;font-family:monospace;font-size:10px; padding:0px; font-weight:bold; text-align:right;">Transaction ID: </td>
			</tr>
			<tr> 
				<td style="border:none;font-family:monospace;font-size:10px; padding:0px; font-weight:bold; text-align:left;">Date:</td> 
			</tr>
		</table>
		<HR/>
	</div>
<center>
	<div id="button2s" style="position:absolute; right:10%; text-align:center;bottom:20%;width:80% !important; height:6%;">
		<hr/>
		<p style="padding-left:35%;text-align:left; font-size:13px; font-weight:bold;font-family:monospace;">Total:   </p>
		

	</div>
	</center>

	<div id="button2s" style="position:absolute; bottom:0%;width:100% !important; height:14%;">
		<input type="button" id="confirmButton" value="CHECK-OUT"/>
		<input type="button" id="cancelButton"value="CANCEL" />
	</div>


</div>

<div id="rightPart">	

    <div class="search-box" 

    style="background-color:#333;
	width:45%;
	height:45px !important;
	margin-left:6px !important;">

        <input type="text"  id="SearchInput" name="searcht"  autocomplete="off" placeholder="Search the products..." required oninvalid="this.setCustomValidity('Please enter valid text')" oninput="setCustomValidity('')"  />
        <img src="images/search1.png" style="height:20px; width:20px;">

<br>
<br>
<br>

        <div style="height:50% !important; position:absolute; margin-top:-15px !important; background-color: rgb(64,46,68) !important; left:0.75%; width:65% !important;"><br></div>
	<div style="height:50% !important; position:fixed; margin-bottom:-15px !important; background-color: rgb(64,46,68) !important; left:21.1%; bottom: 5% !important; width:51.3% !important;"><br></div>

	<div id="searchResult"style="height:400px !important; background-color: rgb(64,46,68) !important;overflow: scroll; left:0.75% !important;position:absolute;  overflow-x:hidden; width:65% !important; scroll-padding: 50px !important;
">
		
   					<table  >
   						<tr style="position:sticky; left:0; top:0 !important;">

   						<th style="width:10%;">Product_ID</th>
   						<th style="width:30%;">Name</th>
   						<th style="width:10%;">In Stock</th>
   						<th style="width:5%;">Price</th>
   						<th style="width:23%;">Description</th>
   						<th style="width:12%;">Action</th>

   					</tr>



   					<div id="searchResult"></div>

    </div>
</body>
</html>

 

 