<?php
/* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
$link = mysqli_connect("localhost", "root", "", "inventorydb");
 
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
 
if(isset($_REQUEST["term"])){
    // Prepare a select statement
    $sql = "SELECT * FROM products WHERE ProductName LIKE ?";
    
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "s", $param_term);
        
        // Set parameters
        $param_term = $_REQUEST["term"] . '%';
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
            
            // Check number of rows in the result set
            if(mysqli_num_rows($result) > 0){
                // Fetch result rows as an associative array
                while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                  
                   $trResult = "
                   <tr row_id='<?php echo $row['ProductID']; ?>'' >
				<td class='td4Ellipsis'><span><?php echo $row['ProductID']; ?></span></td>
				<td class='td4Ellipsis'><span> <?php echo $row['ProductID']; ?></span></td>
				<td class='td4Ellipsis'><span> <?php echo $row['ProductID']; ?></span></td>
				<td class='td4Ellipsis'><span><?php echo $row['ProductID']; ?></span></td>
				<td class='td4Ellipsis'><span><?php echo $row['ProductID']; ?></span></td>
				<td class='td4Ellipsis'><span> <a style='height:20px; width:100%; color:white !important; background-color:#333 !important; font-size:10px !important;' id='selectButton' href='#bottomSelectedProduct?id=<?php echo $row["ProductID"]; ?>' value='SELECT'><?php echo $row['ProductID']; ?></a></span></td> 
			</tr>";
				echo $trResult;
               



                }
            } else{
                echo "<p>No matches found</p>";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
        }
    }
     
    // Close statement
    mysqli_stmt_close($stmt);
}
 
// close connection
mysqli_close($link);
?>