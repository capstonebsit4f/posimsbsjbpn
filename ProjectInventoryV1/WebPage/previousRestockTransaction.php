<?php
require("session.php");
if ($_SESSION['AccountType'] != "AdminLevel"){
	session_destroy();
	echo'<script>alert("You don`t have necessary permission to access this page!"); window.location.href="LoginRegisterPage/login.php";</script>';
	
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Restock Transaction History Page</title>
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/sapooples.css" />

</head>




<body>
<ul> 
	<center>
<div id="hovve" style="position:absolute; left:0%; top:0%; height:100%; width:20%;">  </div>
	<li class = "widthforli"> <a class="inputbuttoon" href="home.php"><img class="notimg"src="images/home1.png"><img class="hoverr"src="images/homes.png"> Home </a></li> 
    <li class = "widthforli"><a class="inputbuttoon"  href="transaction_main.php"><img class="notimg"src="images/formOrder1.png"><img class="hoverr"src="images/formOrders.png">Transaction </a></li> 
    <li class = "widthforli"  id="active"><a class="inputbuttoon"  id="active"  href="inventory.php"><img class="notimg"src="images/inventory1.png"><img class="hoverr"src="images/inventorys.png">Inventory </a></li> 
     <li class = "widthforli"><a class="inputbuttoon"  href="sales.php" ><img class="notimg"src="images/sale1.png"><img class="hoverr"src="images/sales.png">Sales </a></li> 
    <li class = "widthforli"><a class="inputbuttoon"  href="account_main.php"><img class="notimg"src="images/account1.png"><img class="hoverr"src="images/accounts.png">Account </a></li>
    <li class = "widthforli"><a class="inputbuttoon"  href="notification_new.php"><img class="notimg"src="images/notification1.png"><img class="hoverr"src="images/notifications.png">Notifications </a></li>
     <li class = "widthforli" id="cancelledd"><a class="cancelledd" >  </a></li>
    <p class="borderrr">     </p>

</center>

</ul>

<div id="rightPart" style="height:57%;">	

<center>
<br>
<br>
<form action="" method="POST">	
<br>		

<div style="  position:fixed; top:2%; right:0.90%; background-color: rgb(64,46,68) !important;  width:78.2% !important; height:96%;"><br> </div>

	<div id="searchResult"	style="height:88% !important; background-color: rgb(64,46,68) !important;right:0%; width:98.9% !important; position:relative; ">

			<div class="row" style="height:590px !important; margin-top:-3%; background-color:white;  width:96.6%; ">

                <div class="col-lg-12" style="height:97%; background-color:white; width:99.5%; overflow: scroll; overflow-x:hidden; scroll-padding: 50px !important; ">
					<table width="90%"  class="table table-striped table-bordered table-hover" id="historyTable">
   						<tr style="position:sticky; left:0; top:0 !important;">

   						<th style="width:9.5%; text-align:center !important; font-size:13px !important;">Restock_ID</th>
   						<th style="width:9.5%; text-align:center !important; font-size:13px !important;">Account_ID</th>
   						<th style="width:9%; text-align:center !important; font-size:13px !important;">Quantity Added</th>
   						<th style="width:9%; text-align:center !important; font-size:13px !important;">DMGD Quantity</th>
   						<th style="width:9%; text-align:center !important; font-size:13px !important;">unDMGD Quantity</th>
   						<th style="width:12%; text-align:center !important; font-size:13px !important;">No. of Products Added</th>
   						<th style="width:11%; text-align:center !important; font-size:13px !important;">Total</th>
   						<th style="width:15%; text-align:center !important; font-size:13px !important;">Date</th>
   						<th style="width:15%; text-align:center !important; font-size:13px !important;">Action</th>

   					</tr>



<?php
	$conn = mysqli_connect("localhost","root", "");
	$db = mysqli_select_db($conn, 'inventorydb');
	

	$quary = "SELECT * FROM `Restock`";
    	$queryResult = mysqli_query($conn, $quary);
    	
    	if ($queryResult->num_rows >0){
		while($row = $queryResult->fetch_assoc()){

			?>

			<tr>
							<td class="hidden"></td>
							<td><?php echo $row['RestockID']; ?></td>
							<td><?php echo $row['AccountID']; ?></td>
							<td><?php echo $row['RestockTotalQuantity']; ?></td>
							<td><?php echo $row['RestockTotalDMGDProducts']; ?></td>
							<td><?php echo $row['TotalunDMGDQty']; ?></td>
							<td><?php echo $row['CountProduct']; ?></td>
							<td align="right"><?php echo number_format($row['RestockTotalCost'],2); ?></td>
							<td><?php echo date('M d, Y h:i A',strtotime($row['RestockDate'])); ?></td>

							

							<td>
								<a href="#detail<?php echo $row['RestockID']; ?>" data-toggle="modal" class="btn btn-primary btn-sm" style="background-color:#333 !important; color:white; padding:4px; border:none; padding-left:23%; padding-right:23%;">View Details</a>
								<?php include ('fullDetailsRestock.php'); ?>
							</td>
						</tr>
					<?php
					}

		}
	?>
   				</form></center> <br> </div> </div></div>

   			<?php
   

?>


</div>

</div>


</div>


</div>