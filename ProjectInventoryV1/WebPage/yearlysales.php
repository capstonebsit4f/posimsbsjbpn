
<?php
$con  = mysqli_connect("localhost","root","","inventorydb");
 if (!$con) {
     # code...
    echo "Problem in database connection! Contact administrator!" . mysqli_error();
 }else{
          $sql = "SELECT date_format(calendar.datefield,'%M') as date, date_format(calendar.datefield,'%Y') as datee, IFNULL(SUM(coalesce(sales.SalesTotal)),0) AS total_sales FROM sales RIGHT JOIN calendar ON (DATE(sales.SalesDate) = calendar.datefield) WHERE (calendar.datefield <= Date_add(Now(),interval 12 month) and calendar.datefield >= Date_add(Now(), interval - 12 month)) GROUP BY date order by datefield asc LIMIT 12";
        	$result = mysqli_query($con,$sql);
        	 $chart_data="";
         	while ($row = mysqli_fetch_array($result)) { 
 
            $months[]  = $row['date'];
            $total_saless[] = $row['total_sales'];

        }
 
 
 }
 
 
?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sales</title> 
    </head>
    <style>
        #grad3 {
  height: 200px;
  background-color: red; /* For browsers that do not support gradients */
  background-image: linear-gradient(to bottom right, DeepSkyBlue, white);
}

    </style>
    <body>
        <div style="width:31%; height:45%;text-align:center;  bottom: 1.7%; right:1%;  border:5px solid violet; position: absolute; border-color:#402e44;" id="grad3">
            <h2 class="page-header" style="font-size: 15px;" >Yearly Sales Report</h2>
           <p style="font-size: 12px; margin-top: -8px;">
            <?php echo date("Y"); ?> 
        </p>
            <canvas  id="chartjs_bar3"></canvas> 
        </div>    
    </body>
  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">
      var ctx = document.getElementById("chartjs_bar3").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels:<?php echo json_encode($months); ?>,
                        datasets: [{
                            backgroundColor: [
                               "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7",
                                "#AA00D7"
                                
                            ],
                            data:<?php echo json_encode($total_saless); ?>,
                        }]
                    },
                    options: {
                           legend: {
                        display: true,
                        position: 'hidden',
 
                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,
                        }
                    },
 
 
                }
                });
    </script>
</html>