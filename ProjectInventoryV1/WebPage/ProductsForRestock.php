<?php
require("session.php");
if ($_SESSION['AccountType'] != "AdminLevel"){
	session_destroy();
	echo'<script>alert("You don`t have necessary permission to access this page!"); window.location.href="LoginRegisterPage/login.php";</script>';
	


}


$connect = mysqli_connect("localhost", "root", "", "inventorydb");

$sqlGetNotifVal=mysqli_query($connect,"SELECT notifValue as notifVal, notifBool as notifBoolean from `notif` WHERE notifID='1'");
	$rowNotif = mysqli_fetch_assoc($sqlGetNotifVal);
	$notificationValue = $rowNotif["notifVal"];
	$notificationBool = $rowNotif["notifBoolean"];





?>
<!DOCTYPE html>
<html>
<head>
<title>Restock Transaction History Page</title>
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/sapooples.css" />

</head>




<body>
<ul> 
	<center>
<div id="hovve" style="position:absolute; left:0%; top:0%; height:100%; width:20%;">  </div>
	<li class = "widthforli"> <a class="inputbuttoon" href="home.php"><img class="notimg"src="images/home1.png"><img class="hoverr"src="images/homes.png"> Home </a></li> 
    <li class = "widthforli"><a class="inputbuttoon"  href="transaction_main.php"><img class="notimg"src="images/formOrder1.png"><img class="hoverr"src="images/formOrders.png">Transaction </a></li> 
    <li class = "widthforli"  id="active"><a class="inputbuttoon"  id="active"  href="inventory.php"><img class="notimg"src="images/inventory1.png"><img class="hoverr"src="images/inventorys.png">Inventory </a></li> 
     <li class = "widthforli"><a class="inputbuttoon"  href="sales.php" ><img class="notimg"src="images/sale1.png"><img class="hoverr"src="images/sales.png">Sales </a></li> 
    <li class = "widthforli"><a class="inputbuttoon"  href="account_main.php"><img class="notimg"src="images/account1.png"><img class="hoverr"src="images/accounts.png">Account </a></li>
    <li class = "widthforli"><a class="inputbuttoon"  href="notification_new.php"><img class="notimg"src="images/notification1.png"><img class="hoverr"src="images/notifications.png">Notifications </a></li>
     <li class = "widthforli" id="cancelledd"><a class="cancelledd" >  </a></li>
    <p class="borderrr">     </p>

</center>

</ul>

<div id="rightPart" style="height:57%;">	

<center>
<br>
<br>
<form action="" method="POST">	
<br>		

<div style="  position:fixed; top:2%; right:0.90%; background-color: rgb(64,46,68) !important;  width:78.2% !important; height:86%;"><br> </div>

	<div id="searchResult"	style="height:80% !important; background-color: rgb(64,46,68) !important;right:0%; width:98.9% !important; position:relative; ">

		<div style="background-color:white; position:absolute; top:-12%; margin:0; padding:0; height:9%; width:96.5%; left:1.7%; "><p style="font-family:Arial; font-size:20px; font-weight:600; padding-top:12px;"> P R O D U C T S &nbsp;&nbsp;&nbsp; F O R &nbsp;&nbsp;&nbsp; R E S T O C K </p></div>
			<div class="row" style="height:460px !important; margin-top:3%; background-color:white;  width:96.6%; ">

                <div class="col-lg-12" style="height:97%; background-color:white; width:99.5%; overflow: scroll; overflow-x:hidden; scroll-padding: 50px !important; ">
					<table width="90%"  class="table table-striped table-bordered table-hover" id="historyTable">
   						<tr style="position:sticky; left:0; top:0 !important;">

   						<th style="width:9.5%; text-align:center !important; font-size:13px !important;">Product ID</th>
   						<th style="width:9.5%; text-align:center !important; font-size:13px !important;">Product Name</th>
   						<th style="width:9%; text-align:center !important; font-size:13px !important;">Description</th>
   						<th style="width:9%; text-align:center !important; font-size:13px !important;">Category</th>
   						<th style="width:9%; text-align:center !important; font-size:13px !important;">Quantity In Stock</th>
   					</tr>
<?php
		$queryy = "
		  SELECT * FROM `products`  
		  WHERE ProductQuantity BETWEEN 0 AND '".$notificationValue."'
		  ORDER BY ProductQuantity ASC
		  ";

			$result = mysqli_query($connect, $queryy);
			if(mysqli_num_rows($result) > 0)
			{
			 while($row = mysqli_fetch_array($result))
			 {
			?>

			<tr>
							
							<td class="td4Ellipsis" style="width:10% !important;"><span style=" font-size:13px; font-family:Arial;"> <?php echo $row['ProductID']; ?></span></td>
							<td class="td4Ellipsis" style="width:25% !important;"><span style=" font-size:13px; font-family:Arial;"> <?php echo $row['ProductName']; ?></span></td>
							<td class="td4Ellipsis" style="width:40% !important;"><span style=" font-size:13px; font-family:Arial;"> <?php echo $row['ProductDescription']; ?></span></td>
							<td class="td4Ellipsis" style="width:16% !important;"><span style=" font-size:13px; font-family:Arial;"> <?php echo $row['ProductType']; ?></span></td>
							<td class="td4Ellipsis" style="width:9% !important;"><span style=" font-size:13px; font-family:Arial;"> <?php echo $row['ProductQuantity']; ?></span></td>
							
						</tr>
					<?php
					}

		}
	?>
   				</form></center> <br> </div> </div></div>

   			<?php

?>


</div>

</div>


</div>

</div>
</table>
</div>
<button style="background-color:rgb(64,46,68); color:white; position:absolute; bottom:-17.5%; left:35%; width:30%; height:10%; font-size:14px;" type="button" onclick="goToRestock()"> Go To Restock</button>




<script>
	function goToRestock()	{
		window.location.href = "restock.php";
	}
</script>