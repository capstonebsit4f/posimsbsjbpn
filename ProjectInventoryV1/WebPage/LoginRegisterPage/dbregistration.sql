-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 08, 2019 at 12:18 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbregistration`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

DROP TABLE IF EXISTS `tbluser`;
CREATE TABLE IF NOT EXISTS `tbluser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `trn_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`id`, `username`, `email`, `password`, `trn_date`) VALUES
(1, 'lester', 'lester@gmail.com', '25d55ad283aa400af464c76d713c07ad', '2019-03-31 17:28:21'),
(2, 'jessica', 'jessica@yahoo.com', '25d55ad283aa400af464c76d713c07ad', '2019-03-31 17:39:59'),
(3, 'ryle', 'ryle@yahoo.com', '25d55ad283aa400af464c76d713c07ad', '2019-03-31 17:42:51'),
(4, 'jennifer', 'jennifer@yahoo.com', '25d55ad283aa400af464c76d713c07ad', '2019-03-31 17:44:46'),
(5, 'elvie', 'elvie@yahoo.com', '25d55ad283aa400af464c76d713c07ad', '2019-04-01 07:56:04'),
(6, 'resty', 'resty@yahoo.com', '25d55ad283aa400af464c76d713c07ad', '2019-04-01 07:58:46'),
(7, 'wyeth', 'wyeth@yahoo.com', '25d55ad283aa400af464c76d713c07ad', '2019-04-01 08:16:57'),
(8, 'mariella', 'mariella@yahoo.com', '25d55ad283aa400af464c76d713c07ad', '2019-04-01 08:17:41'),
(9, 'ggss', 'ggss@yahoo.com', 'fcea920f7412b5da7be0cf42b8c93759', '2019-04-02 01:03:07'),
(10, 'ggss', 'ggss@gmail.com', '25f9e794323b453885f5181f1b624d0b', '2019-04-02 02:56:41'),
(11, 'jaojohn', 'jaojohn@gmail.com', '25f9e794323b453885f5181f1b624d0b', '2019-04-02 04:50:06');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
