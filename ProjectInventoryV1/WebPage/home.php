<?php
require("session.php");

if ($_SESSION['AccountType'] != "AdminLevel"){
	session_destroy();
	echo'<script>alert("You don`t have necessary permission to access this page!"); window.location.href="LoginRegisterPage/login.php";</script>';
	
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Home Page</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/11home.css" />



<style> 
#i1, #i2, #i3, #i4, #i5{ display: none;}

.container{
    margin: 0 auto;
    margin-left: 286px;
    margin-top: 95px;
    position: relative;
    width: 75.5%;
    height: 0;
    padding-bottom: 38%;
    user-select: none;
    background-color: #1c1c1c;
    border: solid 10px #402e44;
    border-radius:0px ; 
    display:none;
  }

  .container .slide_img{
    position: absolute;
    width: 100%;
    height: 100%;
  }
  .container .slide_img img{
    width: inherit;
    height: inherit;
  }

   .prev{
   	width: 12%;
    height: inherit;
    position: absolute;
    top:0; 
    background-color: rgba(88, 88, 88,.2);
    color:rgba(244, 244, 244,.9);
    z-index: 99;
    transition: .45s;
    cursor: pointer;
    text-align: left;
   }

   .next{
    width: 12%;
    height: inherit;
    position: absolute;
    top:0; 
    background-color: rgba(88, 88, 88,.2);
    color:rgba(244, 244, 244,.9);
    z-index: 99;
    transition: .45s;
    cursor: pointer;
    text-align: center;
  }

  .next{right:0;}
  .prev{left:0;}

  label span{
    position: absolute;
    font-size: 100pt;
    top: 50%;
    transform: translateY(-50%);
  }

  .prev:hover, .next:hover{
    transition: .3s;
    background-color: rgba(88, 88, 89,.4);
    color: #402e44; 
  }

.container #nav_slide{
  width: 100%;
  bottom: 12%;
  height: 11px;
  position: absolute;
  text-align: center;
  z-index: 99;
  cursor: default;
}

#nav_slide .dots{
  top: -5px;
  width: 18px;
  height: 18px;
  margin: 0 4px;
  position: relative;
  border-radius: 100%;
  display: hidden;
  background-color: rgba(0, 0, 0, 0.6);
  transition: .4s;
}

#nav_slide .dots:hover {
  cursor: pointer;
  background-color: rgba(255, 255, 255, 0.9);
  transition: .25s
}

.slide_img{z-index: -1;}

  #i1:checked ~ #one  ,
  #i2:checked ~ #two  ,
  #i3:checked ~ #three,
  #i4:checked ~ #four ,
  #i5:checked ~ #five 
  {z-index: 9; animation: scroll 1s ease-in-out;}

  #i1:checked  ~  #nav_slide #dot1,
  #i2:checked  ~  #nav_slide #dot2,
  #i3:checked  ~  #nav_slide #dot3,
  #i4:checked  ~  #nav_slide #dot4,
  #i5:checked  ~  #nav_slide #dot5
  { background-color: rgba(255,255,255,.9);}

@keyframes scroll{
  0%{ opacity:.4;}
  100%{opacity:1;}
}   

/* .yt{
  margin: 0 auto;
  margin-top: 50px;
  position: relative;
  width: 150px;
  height:50px;
  border: outset #2c2c2c 4px;
  border-radius: 10px;
  text-align: center;
  font-size: 30pt;
  transition: .5s;
}

.yt a{
  text-decoration: none;
  color: #4c4c4c;
  transition: .5s;
}

.yt:hover{
  background: #4c4c4c;
  transition: .3s;
}

.yt:hover a{
  color: #fff;
  transition: .3s;
}
 */
@media screen and (max-width: 685px){
  .container{
    border: none;
    width: 100%;
    height: 0;
    padding-bottom: 55%; 
  } 
  
  label span { font-size: 50pt; }
  
  .prev, .next{
    width: 15%;
  } 
  #nav_slide .dots{
    width: 12px;
    height: 12px;
  }
}
@media screen  and(min-width: 970px){
  .me{ display: none;}
}
</style>

</head>
<body>

<ul> 
	<center>
		
	
<div id="hovve" style="position:absolute; left:0%; top:0%; height:100%; width:20%;">  </div>
	<li class = "widthforli" id="active"> <a class="inputbuttoon" id="active" href="home.php"><img class="notimg"src="images/home1.png"><img class="hoverr"src="images/homes.png"> Home </a></li> 
	<!--<li></li><li></li><li></li>--->
    <li class = "widthforli"><a class="inputbuttoon"  href="transaction_main.php"><img class="notimg"src="images/formOrder1.png"><img class="hoverr"src="images/formOrders.png">Transaction </a></li> 
    <!--<li></li><li></li><li></li>-->
    <li class = "widthforli"><a class="inputbuttoon"  href="inventory.php"><img class="notimg"src="images/inventory1.png"><img class="hoverr"src="images/inventorys.png">Inventory </a></li> 

     <li class = "widthforli"><a class="inputbuttoon"  href="sales.php" ><img class="notimg"src="images/sale1.png"><img class="hoverr"src="images/sales.png">Sales </a></li> 
	<!--<li></li><li></li><li></li>-->
    <li class = "widthforli"><a class="inputbuttoon"  href="account_main.php"><img class="notimg"src="images/account1.png"><img class="hoverr"src="images/accounts.png">Account </a></li>
    <li class = "widthforli"><a class="inputbuttoon"  href="notification_new.php"><img class="notimg"src="images/notification1.png"><img class="hoverr"src="images/notifications.png">Notifications </a></li>
     <li class = "widthforli" id="cancelledd"><a class="cancelledd" >  </a></li>
    <p class="borderrr">     </p>
    <!--<li></li><li></li><li></li>-->

</center>

</ul>


<div id="rightPart" style="height:97%;">	

	

<form action="" method="POST" id="searchPlus">

	<input type="text" id="SearchInput" name="searcht" placeholder="Search the products..." required oninvalid="this.setCustomValidity('Please enter valid text')"
 oninput="setCustomValidity('')" />
	<button type="submit" id="SearchButton" name="submit_search" >

		<img src="images/search1.png" style="height:100%; width:100%;"> </button>

</form>





<?php
	$con = mysqli_connect("localhost","root", "");
	$db = mysqli_select_db($con, 'inventorydb');
	

	$quary = "SELECT `ProductID`, `ProductName`, `ProductDescription`, `ProductPrice`, `ProductType` FROM `products`";
    	$queryResult = mysqli_query($con, $quary);

    	if (!isset($_POST['submit_search'])) 
	{

?>
    	<div id="bannerImg" style="width:76%; height:70.5%; position:fixed; right:2.1%; top:14.6%;"> <img src="images/background.png" style="height:100%; width:100%; "/> </div> 

<div id="logout_account"  style="position:fixed; width:28% !important;height:5.5%; right:26%; bottom:7%;" >
	<button type="reset" onclick="hideCategories()" style="border-radius:20px; border:3px solid rgb(45,25,40); position:absolute; width:100% !important; height: 100%; top:0%; right:0%; background-color:white;">  

		<span style="left:-1%; position: absolute; top:15%; height:50%; width:100% !important; overflow:hidden !important; font-size:14px !important; font-family:monospace !important; font-weight:bold;"> CLICK TO SEE CATEGORIES </span> </button> 
	 </div>

    	<?php
}
	if (isset($_POST['submit_search'])) 
	{
		$searcht = stripslashes($_POST['searcht']);
		$searcht = str_replace('  ', ' ', $searcht);

		$quary = "SELECT * FROM `products` WHERE CONCAT (`ProductID`, `ProductName`, `ProductDescription`, `ProductPrice`, `ProductType`) LIKE '%".$searcht."%'";
    	$queryResult = mysqli_query($con, $quary);
    	$rowCount = mysqli_num_rows($queryResult);

		if($searcht === '' or $searcht === ' ' or $searcht ==="  "){
			echo"<script> alert('Please enter correct keywords / text.');  window.location.href='home.php'; </script>";


		}

		if ($searcht != '' && $searcht != ' '){

			?>
			

			<div id="bannerImg" style="width:76%; height:70.5%; position:fixed; right:2.1%; top:14.6%;"> <img src="images/background.png" style="height:100%; width:100%; "/> </div>


<div id="logout_account"  style="position:fixed; width:28% !important;height:5.5%; right:26%; bottom:7%;" >
	<button type="reset" onclick="hideCategories()" style="border-radius:20px; border:3px solid rgb(45,25,40); position:absolute; width:100% !important; height: 100%; top:0%; right:0%; background-color:white;">  

		<span style="left:-1%; position: absolute; top:15%; height:50%; width:100% !important; overflow:hidden !important; font-size:14px !important; font-family:monospace !important; font-weight:bold;"> CLICK TO SEE CATEGORIES </span> </button> 
	 </div>



			<center>
			<br>
			<br>
			<form action="" method="POST">	
			<br>
			
<div style=" position:absolute; top:14%; right:2.1%; background-color: rgb(64,46,68) !important;  width:97% !important; height:85%;"><br> </div>

<button style="position:fixed; right:37%; top:1%; background-color: rgb(244,102,102); color:white; height:7%; border:none; font-size:11px;" id="closeBtns" onclick="this.parentNode.remove()" type="reset">Close Result</button>

	<div id="searchResult"	style="height:80.5% !important; background-color: rgb(64,46,68) !important;right:2.1%; width:97% !important;  bottom:5%; position:absolute; ">	


					<div id="bannerImg" style="width:76.7%; height:78.5%; position:fixed; right:2.1%; top:14.6%; background-color:rgb(64,46,69) !important;">  </div>

					<div style="height:96% !important; background-color:white; right: 1.7%; bottom:-0.5%; position:absolute; width:96.6%; z-index:0;" ></div> 

		<div style="height:94.7%; background-color:white; right: 1.75%; top:5%; position:absolute; width:96.5%; z-index:0; overflow: scroll;overflow-x:hidden; scroll-padding: 50px !important; ">

   					<table  style="margin-left:0.2% !important; margin-top:0.2% !important; position:relative; z-index:10; width:99.6%;  ">
   						<tr style="position:sticky; left:0; top:0 !important;">

			   					
			   						<th style="width:10%;">Product_ID</th>
			   						<th style="width:30%;">Name</th>
			   						<th style="width:40%;">Description</th>
			   						<th style="width:8%;">Price</th>
			   						<th style="width:12%;">Category</th>
			   						

			   					</tr>	

			   	


			<?php

		

   		while ($row = mysqli_fetch_array($queryResult))
   		 {
   		 	$rowCount = mysqli_num_rows($queryResult);
   		 	echo "<tr row_id='" . $row['ProductID'] . "'><td class='td4Ellipsis'><span>". $row['ProductID']. "</span></td>" ."<td class='td4Ellipsis'><span>". $row['ProductName']. "</span></td>" ."<td class='td4Ellipsis'><span>". $row['ProductDescription']. "</span></td>" ."<td class='td4Ellipsis'><span>". $row['ProductPrice']. "</span></td>" ."<td class='td4Ellipsis'><span>". $row['ProductType']. "</span></td>";
		}
		echo  "<div style='position:fixed; left:21%; top:10.5% !important; font-size:13px; font-family:monospace;'> Showing " .$rowCount.  " results.  </div>";
		echo "</table>";
		
		

	?>
   				</form></center> <br></div> 

   			<?php
   		}
	
}else{

	?>

	<div id="bannerImg" style="width:76%; height:70.5%; position:fixed; right:2.1%; top:14.6%;"> <img src="images/background.png" style="height:100%; width:100%; "/> </div>
</div>

<div id="categorybtnsdiv"  style="position:fixed; width:28% !important;height:5.5%; right:26%; bottom:7%;" >
	<button type="reset" onclick="hideCategories()" style="border-radius:20px; border:3px solid rgb(45,25,40); position:absolute; width:100% !important; height: 100%; top:0%; right:0%; background-color:white;">  

		<span style="left:-1%; position: absolute; top:15%; height:50%; width:100% !important; overflow:hidden !important; font-size:14px !important; font-family:monospace !important; font-weight:bold;"> CLICK TO SEE CATEGORIES </span> </button> 
	 </div>


	<?php 
}

?>






<div id="logout_account"  style="position:fixed; width:18% !important;height:12%; right:2.1%; top:1%;" >
	<div id="logout_dropdown"    style="position:absolute; width:98% !important;  bottom: 0%; height: 60%; right:0%; background-color:rgb(45, 25, 40); display:none;"> <a href="logout.php" style=" font-size:15px; color: white; font-weight:bold; font-family:monospace; text-align:center; width:100%; text-decoration:none; position:absolute; bottom:15%; margin-bottom:0px !important;" >L O G O U T</a></div> 

	<button type="reset" onclick="hideLogout()" style="border-radius:20px; border:3px solid rgb(45,25,40); position:absolute; width:100% !important; height: 60%; top:0%; right:0%; background-color:white;"><img src="images/acc1.png" style="height:105%; width:20%; position:absolute;top:-1%; right:-4%; "/>  <span style="position: absolute;left:0%; top:30%; height:45%; width:80% !important; overflow:hidden !important; font-size:15px !important; font-family:monospace !important; font-weight:bold;"> Hello, <?php echo $_SESSION['username']; ?></span> </button> 

	 </div>


<!--home category-->
<div class="container" id="container">


	<button style="position:fixed; right:35%; top:1%; background-color: rgb(244,102,102); color:white; height:7%; border:none; font-size:11px;" id="closeBtns" onclick="hideCategories()" type="button">Close Categories</button>
  
  <input type="radio" id="i1" name="images" checked />
  <input type="radio" id="i2" name="images" />
  <input type="radio" id="i3" name="images" />
  <input type="radio" id="i4" name="images" />
  <input type="radio" id="i5" name="images" />  
  
  <div class="slide_img" id="one" style="background-color:#402e44;"> 
  		
      	<img src="images/cate/balloons.jpg" style="width: 390px; height: 230px; margin-top: 18px; margin-left: 95px;">
      	<img src="images/cate/caketopper.jpg" style="width: 390px; height: 230px; margin-top: 10px; margin-left: 55px;">
      	<img src="images/cate/Banner.jpg" style="width: 390px; height: 230px; margin-top: 10px; margin-left: 95px;">
      	<img src="images/cate/PartyFlags.jpg" style="width: 390px; height: 230px; margin-top: 20px; margin-left: 55px;">
     
      
        <label class="prev" for="i3"><span>&#x2039;</span></label>
        <label class="next" for="i2"><span>&#x203a;</span></label>  
    
  </div>
  
  <div class="slide_img" id="two" style="background-color:#402e44;">
    <img src="images/cate/candlee.jpg" style="width: 390px; height: 230px; margin-top: 18px; margin-left: 95px;">
      	<img src="images/cate/FoilBalloonCursive.jpg" style="width: 390px; height: 230px; margin-top: 10px; margin-left: 55px;">
      	<img src="images/cate/Hats.jpeg" style="width: 390px; height: 230px; margin-top: 10px; margin-left: 95px;">
      	<img src="images/cate/LBags.jpg" style="width: 390px; height: 230px; margin-top: 20px; margin-left: 55px;">
     
      
        <label class="prev" for="i1"><span>&#x2039;</span></label>
        <label class="next" for="i3"><span>&#x203a;</span></label>
    
  </div>
      
  <div class="slide_img" id="three" style="background-color:#402e44;">
 
      <img src="images/cate/plates.jpg" style="width: 390px; height: 230px; margin-top: 18px; margin-left: 95px;">
      	<img src="images/cate/tablecover.jpg" style="width: 390px; height: 230px; margin-top: 10px; margin-left: 55px;">
      	<img src="images/cate/partypoppers.png" style="width: 390px; height: 230px; margin-top: 10px; margin-left: 95px;">
      	<img src="images/cate/FoilCurtain.png" style="width: 390px; height: 230px; margin-top: 20px; margin-left: 55px;">
      
        <label class="prev" for="i2"><span>&#x2039;</span></label>
        <label class="next" for="i1"><span>&#x203a;</span></label>
  </div>

 

  <div id="nav_slide">
    <label for="i1" class="dots" id="dot1"></label>
    <label for="i2" class="dots" id="dot2"></label>
    <label for="i3" class="dots" id="dot3"></label>
    <label for="i4" class="dots" id="dot4"></label>
    <label for="i5" class="dots" id="dot5"></label>
  </div>
    
</div>




<script>
function hoverrHome(element){
	element.setAttribute('src','images/sale.png' )
}

function unhoverrHome(element){
	element.setAttribute('src','images/home.png' )
}

function hideLogout(){
	var logout = document.getElementById('logout_dropdown');
	if (logout.style.display === 'block'){
	logout.style.display = 'none'; 
	}else{
	logout.style.display = 'block'; 	
	}
}

function hideCategories(){
	var categoryDiv = document.getElementById('container');
	if (categoryDiv.style.display === 'none'){
	categoryDiv.style.display = 'block'; 
	}else{
	categoryDiv.style.display = 'none'; 	
	}
	
}

</script> 


