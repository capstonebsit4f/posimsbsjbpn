<?php
//fetch.php
$connect = mysqli_connect("localhost", "root", "", "inventorydb");
$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
  SELECT * FROM `products` 
  WHERE ProductName LIKE '%".$search."%'
  OR ProductID LIKE '%".$search."%' 
  OR ProductPrice LIKE '%".$search."%' 
  OR ProductType LIKE '%".$search."%' 
  OR ProductQuantity LIKE '%".$search."%'
 ";
}
else
{
 $query = "
  SELECT * FROM `products` ORDER BY ProductID
 ";
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <div class="table-responsive">
   <table class="table table bordered">
    <tr>
    <th style="width:10%;">Product_ID</th>
   	<th style="width:30%;">Name</th>
   	<th style="width:10%;">In Stock</th>
   	<th style="width:5%;">Price</th>
   	<th style="width:23%;">Description</th>
   	<th style="width:12%;">Action</th>
    </tr>
 ';
 while($row = mysqli_fetch_array($result))
 {
 	$rowCount = mysqli_num_rows($result);
  $output .= '
   <tr>
    <td class="td4Ellipsis"><span>'.$row["ProductID"].'</span></td>
    <td class="td4Ellipsis"><span> '.$row["ProductName"].'</span></td>
    <td class="td4Ellipsis"><span> '.$row["ProductQuantity"].'</span></td>
    <td class="td4Ellipsis"><span> '.$row["ProductPrice"].'</span></td>
    <td class="td4Ellipsis"><span> '.$row["ProductDescription"].'</span></td>
    <td class="td4Ellipsis"><span> <button style="height:20px; width:100%; color:white !important; background-color:#333 !important; font-size:10px !important;" id="selectButton" onclick="clicked()" value="'.$row["ProductID"].'">SELECT</button></span></td>

   </tr>

   <div style="position:fixed; left:21%; top:10% !important; font-size:13px; font-family:monospace;"> Showing ' .$rowCount.  ' results.  </div>

  ';
 }
 echo $output;
}
else
{
 echo '<div style="position:fixed; top: 10%; left:21%; font-size:13px; font-family:monospace;"> Data Not Found</div>';
}

?>