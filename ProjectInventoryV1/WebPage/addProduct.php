
<?php

include('session.php');
// initializing variables
$item_name = "";
$item_price    = "";
$quant  = "";
$idu="";
$cost="";
$Pre="";
$try="";
$tri="";

// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'inventorydb');
if (mysqli_connect_errno())
    {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

// Add item
if (isset($_POST['add'])) {
  // receive all input values from the form
  
 $item_name=mysqli_real_escape_string($db, $_POST['product_name']);
  $quant=mysqli_real_escape_string($db, $_POST['quant']);
  $idu=mysqli_real_escape_string($db, $_POST['sup_id']);
  $cost=mysqli_real_escape_string($db, $_POST['sup_cost']);
  $Pre= mysqli_real_escape_string($db, $_POST['price']);
  $try=mysqli_real_escape_string($db, $_POST['cate']);
  $tri=mysqli_real_escape_string($db, $_POST['descr']);

    $querys = "INSERT INTO product_temp(ProductName,ProductQuantity,SupplierID,ProductType,ProductDescription,ProductPrice,ProductSupplierCost,AccountID) 
  			  VALUES('$item_name','$quant', '$idu','$try','$tri','$Pre','$cost','".$_SESSION['id']."')";

      if(mysqli_query($db, $querys))
      {
      echo "<script>alert('Added To List!'); window.location.href='addProduct.php';</script>";
    }
    else{
        echo"<script>alert('Something wrong!!!');</script>";
        echo mysqli_error($db);

    }
  	
  	
  
}
?>

<!DOCTYPE html>
<html>
<head>
<title>Add Product Page</title>
<link rel="stylesheet" href="css/addProduct.css" />

<style> 
li a{height:65px !important;}

</style>
</head>
<body>

<ul> 
	<center>
<div id="hovve" style="position:absolute; left:0%; top:0%; height:100%; width:20%;">  </div>
	<li class = "widthforli"> <a class="inputbuttoon" href="home.php"><img class="notimg"src="images/home1.png"><img class="hoverr"src="images/homes.png"> Home </a></li> 
	<!--<li></li><li></li><li></li>--->
    <li class = "widthforli"><a class="inputbuttoon"  href="transaction_main.php"><img class="notimg"src="images/formOrder1.png"><img class="hoverr"src="images/formOrders.png">Transaction </a></li> 
    <!--<li></li><li></li><li></li>-->
    <li class = "widthforli"  id="active"><a class="inputbuttoon" id="active"  href="inventory.php"><img class="notimg"src="images/inventory1.png"><img class="hoverr"src="images/inventorys.png">Inventory </a></li> 

     <li class = "widthforli" ><a class="inputbuttoon"  href="sales.php" ><img class="notimg"src="images/sale1.png"><img class="hoverr"src="images/sales.png">Sales </a></li> 
	<!--<li></li><li></li><li></li>-->
    <li class = "widthforli"><a class="inputbuttoon"  href="account_main.php" ><img class="notimg"src="images/account1.png"><img class="hoverr"src="images/accounts.png">Account </a></li>
    <li class = "widthforli"><a class="inputbuttoon"  href="notification_new.php" ><img class="notimg"src="images/notification1.png"><img class="hoverr"src="images/notifications.png">Notifications </a></li>
     <li class = "widthforli" id="cancelledd"><a class="cancelledd" >  </a></li>
    <p class="borderrr">     </p>
    <!--<li></li><li></li><li></li>-->

</center>

</ul>
<div class="rightPart" style="height:99%; width:79%">
       <div class="rightTop">     
     <h1 style="text-align:center; border:5px solid white;margin:1%; padding:7px; color:white; font-family:arial; font-size:20px; font-weight:bold; word-spacing:0.5px;">P R O D U C T  &nbsp;&nbsp;  I N F O R M A T I O N </h1>
     <br>
      <h1 style="position:absolute;top:13.5%; text-align:left; margin:1%; padding:1px; color:white; font-family:arial; font-size:14px; font-weight:100; font-style:italic; word-spacing:0.5px;">PLEASE FILL-UP ALL REQUIRED INFORMATION</h1>     

<form method="POST" action="addProduct.php">

	<div class="inputTop">
    
    <div class="inputTopUp"><input type="text" name="product_name"  placeholder="Product Name"></div>
    <div class="inputTopMid"> 
    	<input type="number"  name="price" placeholder="Price" style="margin-left:0px !important;"> 
    	<input type="number" name="sup_cost" placeholder="Supplier Cost">

   		<button type="button" onclick="inc()" style="width: 50px; height: 55.5px; background-color:#ddd; color:black; 
	margin-right:-8px !important; padding:0;">+</button>

   		<input type="number" name="quant" id="quant" min="1" max="" placeholder="Quantity" style="width:15%;" value="<?php echo  $quant;?>">

   		<button type="button" onclick="dec()" style="width: 50px; height: 55.5px;  background-color:#ddd; color:black; margin-left:-8px !important; padding:0;">-</button>
	        <script type="text/javascript">
	            function inc() {
	                document.getElementById('quant').stepUp();
	            }
	            function dec() {
	                document.getElementById('quant').stepDown();
	            }
	          </script> 
      </div>

    	<div class="inputTopDown"><input type="text"  name="descr" placeholder="Please insert short description about the product"> </div>
    </div>



<!--Middle Buttons -->
<div class="buttonMid">
<div style="height:17%; position:absolute; top:0%; width:100%;">
	
 <input type="text" name="cate" placeholder="Category">
</div>
<div style="height:17%; position:absolute; top:25%; width:100%;">
	
<input type="text"  name="sup_id" placeholder="Supplier ID/CODE">
</div>



 <div style="height:15%; position:absolute; bottom:0%; width:100%;"><button id="buttonMidTop" type="submit" name="add">ADD TO LIST OF NEW PRODUCT</button></div>

<div style="height:15%; position:absolute;  top:55%; width:100%;"><a id="buttonMidTop" href='addSupplier.php'>CLICK HERE TO ADD NEW SUPPLIER</a></div>


<div style="height:15%; position:absolute;  top:67.5%;width:100%;"><button id="buttonMidBottom" onclick="return confirm('Are you sure?')">CLEAR INFORMATION</button></div>

</div>

<script type="text/javascript">
    var b;
    document.getElementById('product_name', 'quant', 'descr', 'sup_id', 'sup_cost','cate','Price' ).reset();
</script>
</div>           
</div>

    <div class="resultLowPart">
    	<h4 class="header-title">LIST OF NEW PRODUCT/S</h4>
        <div class="LowPartBorder">
	    
			<table class="table text-center" style="position: absolute;width:100% !important; overflow:hidden;">
                <tr style="width:10%; overflow:scroll;">
  					<th style="width:22% !important;">PRODUCT NAME</th>
                    <th style="width:12% !important;">SUPPLIER ID</th>
                    <th style="width:25% !important;">DESCRIPTION</th>
                    <th style="width:10% !important;">PRICE</th>
                    <th style="width:8% !important;">QUANTITY</th>
                    <th style="width:15% !important;">CATEGORY</th>
 					<th style="width:8% !important;">RMV</th>          
                </tr>
			<?php 
               $conn = new mysqli("localhost","root","","inventorydb");
               $sql = "SELECT * FROM product_temp where AccountID='".$_SESSION['id']."'";
               $result = $conn->query($sql);
					$count=0;
               if ($result -> num_rows >  0) {
                 while ($row = $result->fetch_assoc()) 
				 {
					  $count=$count+1;
                   ?>
                   <tr style="width:10%; overflow:hidden;">
                    <input type="hidden" name="" value="<?php echo $count ?>" >
                     
                      <td  class="td4Ellipsis"> <span style="text-align:left;"> <?php echo $row["ProductName"] ?></span></td>
                      <td  class="td4Ellipsis"> <span style="text-align:center;"> <?php echo $row["SupplierID"]  ?></span></td>
                      <td  class="td4Ellipsis"> <span> <?php echo $row["ProductDescription"]  ?></span></td>
                      <td  class="td4Ellipsis"> <span> <?php echo $row["ProductPrice"]  ?></span></td>
                      <td  class="td4Ellipsis"> <span> <?php echo $row["ProductQuantity"]  ?></span></td>
                      <td  class="td4Ellipsis"> <span> <?php echo $row["ProductType"]  ?></span></td>

                      <input type="hidden" name="" value="<?php echo $row['Product_tempID']  ?>">
                     <td  class="td4Ellipsis"> <span>  <a style="color: red;" href="deleteProduct.php?id=<?php echo $row['Product_tempID'] ?>" >X</a></span></td>
                     
                 </tr> 
                    
            <?php
                 
                 }
               }

            ?>
        
            
            
                                            </tbody>
                                        </table>
           
                                    </div>
                                </div>
                    <!-- Contextual Classes end -->
                 </form>
        <!-- main content area end -->
<div class="buttonBottom">
<div id="button2s" style="position:absolute; bottom:0%;right:0%; width:100% !important; height:100%;">
		<button type="reset" id="confirmButton" onclick="showConfirm()"> ADD NEW <BR> PRODUCT/S </button>
		<button type="reset" id="cancelButton" onclick="showCancel()"> CANCEL </button>
	</div>
</div>


</div>
	<div id="PreventClick" style="position:fixed; top:0%; left:0%; height:100%; width:100%; display:none;" > 	
	   <div id="CancelOrder" style="display:inherit; position:fixed; left:40%; top:30%; height:30%; width:25%; background-color: white; border:2px solid black;   box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.7); " > 
		<center><p style="position:absolute; top:32%;left:0%; text-align:center; width:100%; font-family:arial; font-size:13.5px;"> Are you sure you want to cancel transaction? <br> <br>This action will empty your List</p></center>
		<button id="redButton" onclick="cancelShowCancel()">NO</button>
		<button id="greenButton" onclick="cancelCheckOut()">YES</button>
	</div>
</div>

<div id="PreventClickAdd" style="position:fixed; top:0%; left:0%; height:100%; width:100%; display:none;" > 	
   <div id="GoToOrder" style="display:inherit; position:fixed; left:40%; top:30%; height:30%; width:25%; background-color: white; border:2px solid black;   box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.7); " > 
   		<center><p style="position:absolute; top:32%;left:0%; text-align:center; width:100%; font-family:arial; font-size:13.5px;">Are you sure?</p></center>
		<button id="redButton" onclick="cancelShowConfirm()">NO</button>
		<button id="greenButton" onclick="goToCheckOut()">YES</button>
	</div>
</div> 

<div id="emptyCartAlert"></div> 

</html>

<script>

	function cancelCheckOut()	{
		window.location.href = "emptyCartProduct.php";
	}		
	
	function showCancel(){
    	var cancel = document.getElementById('PreventClick');
		if (cancel.style.display === 'none'){
		cancel.style.display = 'block'; 
		}
		}

    function cancelShowCancel(){
    	var cancel = document.getElementById('PreventClick');
		if (cancel.style.display === 'block'){
		cancel.style.display = 'none'; 
		}
		}

	function showConfirm(){
    	var confirm = document.getElementById('PreventClickAdd');
		if (confirm.style.display === 'none'){
		confirm.style.display = 'block'; 
		}
		}

    function cancelShowConfirm(){
    	var confirm = document.getElementById('PreventClickAdd');
		if (confirm.style.display === 'block'){
		confirm.style.display = 'none'; 
		}
		}	

	function goToCheckOut()	{
		window.location.href = "AddProductToDB.php";
	}

</script>	