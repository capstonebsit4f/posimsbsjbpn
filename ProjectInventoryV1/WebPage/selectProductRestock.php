<style>
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button{
	-webkit-appearance:none;
	margin:0;

}
</style>

<?php

include("session.php");
 $connectss = new mysqli("localhost","root", "", "inventorydb");
 	
  if($stmt = $connectss->prepare("SELECT `ProductID`,`ProductName`, `ProductDescription`, `ProductPrice`, `ProductQuantity`  FROM `products` WHERE `ProductID` = ?")){ 
    $stmt->bind_param("s", $_GET["idRow"]); /* BIND THE PASSED ON DATA TO THE SELECT QUERY */
    $stmt->execute(); /* EXECUTE THE QUERY */
    $stmt->bind_result($ProductID,$ProductName, $ProductDescription, $ProductPrice, $ProductQuantity); /* BIND THE RESULT TO THESE VARIABLES */
    $stmt->fetch(); /* FETCH THE RESULTS */
    $total = 0;

    /* WHAT YOU ECHO IS WHAT WILL BE RETURNED TO THE AJAX REQUEST */
    echo '<div id="bottomHidden"  style="position:absolute; left:3%; top:0% !important; width:78% !important; height: 100% !important;background-color: white; "> 

					<div id="hiddenSelectedItem" style="height: 56%; width: 96.5%; left:-2%; top:5%; position:absolute; border:2px solid black; ">
					
					<p style="text-align:left; padding-left:2%;padding-bottom:1.5%;padding-top:1%; font-size:17px; font-family:monospace; font-weight:bold;">SELECTED PRODUCTS</p>
					<table style="width:95% !important; left:0%; position:absolute; border:0px !important;">

						<tr style="border:0px !important; text-align:left;">
							<td style="font-weight:bold;padding:0px; padding-bottom: 0px !important;width:20% !important; border-left:0px !important; border-right:0px !important; ">ProductID</td>
							<td style=" font-weight:bold;padding:0px; padding-bottom: px !important;text-align:center; width:60% !important; border-left:0px !important; border-right:0px !important; ">Product Name</td>
							<td style="font-weight:bold;padding:0px; padding-bottom: 0px !important;width:15% !important; border-left:0px !important; border-right:0px !important; ">Price</td>
						</tr>

						<tr style="border:0px !important; text-align:left;">
							<td style="padding:0px; width:20% !important; border-left:0px !important; border-right:0px !important; ">'.$ProductID.'</td>
							<td style=" padding:0px; text-align:center; width:60% !important; border-left:0px !important; border-right:0px !important; ">'.$ProductName.'</td>
							<td style="padding:0px; width:15% !important; border-left:0px !important; border-right:0px !important; ">'.$ProductPrice.'</td>
						</tr>


					</table>
					<div style="right:30%; bottom:2%; position:absolute; font-size:13px; font-weight:bold; font-family:monospace;">Total:  </div>
					<div style="right:12%; bottom:2%; position:absolute; font-size:13px; font-weight:bold; font-family:monospace;" id="totalDiv"></div>
				</div>

				<div style="position:absolute; left: -2%;bottom:2.2%; height:23%; width:97.5%;">

				<div style="height:49.5%; width:35%; background-color:rgb(64, 46, 68); color:white; font-family:arial; font-size:12px; position:absolute; left:0%; text-align:center; font-weight:bold; padding-top:2%;">PRODUCT STATE: </div> 
				<input style="height:78%; width:45%; background-color:white; color:black; font-family:arial; font-size:13px; position:absolute; right:20%;" disabled placeholder="No. of Damaged Product/s: " /> 
				<input type="number" style="height:78%; width:20%; background-color:white; color:black; font-family:arial; font-size:13px; position:absolute; right:0%;" placeholder="Quantity: " value="0" id="stateQty"/> 
				</div>
				<div style="position:absolute; left: -2%;bottom:15%; height:20%; width:97.5%; font-family:monospace; font-size:12px;">
				*NOTE: If there is no damaged product, do not insert the quantity*
				</div>
				 </div>

				 <div style="position:absolute; right:1.75%; top:5% !important; width: 19% !important; height:22% !important; overflow:hidden;">
				 <input type="number" id="UnitCost" placeholder="Unit Cost" style="position: absolute; top:0.75% ; right:0%; width:49.5% !important; height:100% !important;  font-size:11px; font-family:arial;" required/>
				 <input required type="number" id="SupplierID" placeholder="SupplierID" style="position: absolute; top:0.75% ; right:50%; width:49.5% !important; height:100% !important;  font-size:11px; font-family:arial;"/>
				 </div> 

				 <div id="quantityADDCart"style="position:absolute; right:1.75%; top:32% !important; width: 19% !important; height:63% !important; overflow:hidden;">

				 	<p  style=" font-size:10px; font-weight:bold; font-family:monospace; padding-bottom:5% !important; padding-top:0% !important; color:black;text-align:left;">QUANTITY</p>
				 	<div >

						<input type="button" class="add" value="+" style=" font-size:16px; padding-bottom:17% !important; padding-top:2% !important; position: absolute; width:21% !important; left:0% !important; height:15px !important; top:13.75% !important; background-color:rgb(64,46,68); color:white;"/>  		
						<input type="number" id="quantity" value="0" style="position: absolute; top:13.75% ; right:24.5%; width:52% !important; height:28px !important; "/>

						<input type="button" class="sub" value="-" style=" font-size:16px; padding-bottom:17% !important;position: absolute; width:21% !important; right:0% !important; height:15px !important; top:13.75% !important; background-color:rgb(64,46,68); color:white;"/> 
				</div>


				<button type="button" name="add_to_cart" id="add_to_cart" class="add_to_cart" value="'.$ProductID.'" style="font-weight:bold; position: absolute; width:100% !important; right:0% !important; height:50px !important; bottom:3.75% !important; background-color:rgb(64,46,68); color:white;" >ADD TO CART</button> 
				</div>
			</div>

';
$stmt->close();
  } /* END OF PREPARED STATEMENT */
else{
	echo'errorrr';
}


?>

<script>

var total =0;
var subtotal = 0;
var totals = 0;

$('#UnitCost').on('change', function() {  
	subtotal = document.getElementById("quantity").value * $(this).val();
	totals = subtotal;
	document.getElementById("totalDiv").innerHTML=totals + " Php";
});


// if user changes value of quantity
$('#quantity').change(function() {  

	subtotal = document.getElementById("quantity").value * document.getElementById("UnitCost").value;
 totals = subtotal;
document.getElementById("totalDiv").innerHTML=totals + " Php";
}).trigger('change');



$('.add').click(function() {
  var target = $('#quantity', this.parentNode)[0];
  target.value = +target.value + 1;

  	subtotal = document.getElementById("quantity").value * document.getElementById("UnitCost").value;
  	totals = subtotal;
  	document.getElementById("totalDiv").innerHTML=totals + " Php";
  
});

$('.sub').click(function() {
  var target = $('#quantity', this.parentNode)[0];
  if (target.value > 0) {
    target.value = +target.value - 1;

    subtotal =document.getElementById("quantity").value * document.getElementById("UnitCost").value;
  	totals = subtotal; 
  	document.getElementById("totalDiv").innerHTML=totals + " Php";
  }

 
});

document.getElementById("totalDiv").innerHTML=totals + " Php";


$(function(){
		$('.add_to_cart').dblclick(false);
		});

$(document).on('click', '.add_to_cart', function(){
	
		var pid=$(this).val();
		var quanty=$('#quantity').val();
		var price = $('#quantity').val() * $('#UnitCost').val();
		var supplierID = $('#SupplierID').val();
		var UnitCost = $('#UnitCost').val();
		var stateQty = $('#stateQty').val();
		
		if(quanty > 0 && UnitCost > 0 && supplierID > 0){
			if(stateQty > quanty){
				alert("Quantity of damaged product is greater than the total quantity!");
				
			}
			else{
			$.ajax({
				url:"add_cart_restock.php",
				method:"POST",
				data:{
					pid: pid,
					quanty: quanty,
					price : price,
					UnitCost :UnitCost,
					supplierID :supplierID,
					stateQty: stateQty,
					
				},
				success:function(result){
					$("#tableCart").html(result);	
					$("#quantity").val() = 0;					
					}
			});

			
		}	
		}
		else if (quanty == 0 && UnitCost ==0 && supplierID == 0 ){
			alert('Please enter SupplierID, Unit Cost and Quantity');
			
		}

		else if(quanty=="" || quanty == 0 ){
			alert('Please enter Quantity');
			
			
		}
		else if (UnitCost =="" || UnitCost ==0){
			alert('Please enter Unit Cost');
			
		}
		else if (supplierID =="" || supplierID ==0){
			alert('Please enter Supplier ID');
			
		}

		else{
			alert('Please Enter a Numeric Value');
			
		}



	});
	

</script>