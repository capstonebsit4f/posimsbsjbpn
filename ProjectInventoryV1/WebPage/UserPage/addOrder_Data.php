<?php
	include('session.php');
	if(isset($_POST['check'])){
		?>

	
		<table width="100%" style="border:none !important;" class="noBorderCheckOutTbl">
			<thead>
				<th>Action</th>
				<th>Product Name</th>
				<th>Available Qty</th>
				<th>Product Price</th>
				<th>Purchase Qty</th>
				<th>SubTotal</th>
			</thead>
			<tbody>
			<form method="POST" action="">
			<?php
				$total=0;
				$query=mysqli_query($conn,"select * from cart left join products on products.ProductID=cart.ProductID where AccountID='".$_SESSION['id']."'");
				while($row=mysqli_fetch_array($query)){
					?>
					<tr>
						<td><button type="button" class="remove_prod" value="<?php echo $row['ProductID']; ?>"><i></i> Remove</button></td>
						<td><?php echo $row['ProductName']; ?></td>
						<td><?php echo $row['ProductQuantity']; ?></td>
						<td align="right"><?php echo number_format($row['ProductPrice'],2); ?></td>
						<td align="left">
							<button type="button" class="minus_qty2" value="<?php echo $row['ProductID']; ?>"style=" font-size:14px; width:20% !important; height:10% !important;  background-color:rgb(64,46,68); color:white;"><i> - </i></button> 

							<button type="button"class="add_qty2" value="<?php echo $row['ProductID']; ?>"style=" font-size:14px;  width:20% !important;  height:10% !important; background-color:rgb(64,46,68); color:white;"><i>+</i></button> 
							
							<span align="right" style=" text-align:right; padding-left:15% !important;"><?php echo $row['qty'];?></span>
													</td>
						<td align="right"><strong><span><?php echo $row['qtyPrice']?></span></strong></td>
					</tr>
					<?php

					
					$sumQuery = 	mysqli_query($conn,"select SUM(qtyPrice) AS sum_value from `cart` where AccountID='".$_SESSION['id']."'");
					$rowss = mysqli_fetch_assoc($sumQuery);
					$sum = $rowss["sum_value"];
					if($sum === "0"){
						$sum = 0;
					}

				}
			?>
			<tr class="bottomCheckOutTbl">
				<td colspan="5" align="right"><span style="font-weight:bold;">Grand Total</span></td>
				<td align="right"><strong><span id="totalP1">  </span><strong></td>
			</tr>
			</form>
			</tbody>
		</table>

		<?php
	}
?>

<script> 

	$("#totalP1").text("<?php echo number_format($sum,2); ?>"); 

</script>